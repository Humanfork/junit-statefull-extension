JUnit Statefull Extension
=========================

It is a JUnit extension that provides a platform to handle reusable statefull resources.
 
- Statefull means that the resource instances will be created once for every test class instead of every test case.
- Reusable means that you write the resource handler code once, and then use it in different test classes.

The resource handler have to implement the `de.humanfork.junit.statefullextension.StatefullTestLifeCycle` interface,
that contains some methods which will be invoked by the runner.

Maven
-----
Available from [Maven Central](https://search.maven.org/search?q=g:de.humanfork.junit.statefullextension)

[![Maven Central](https://maven-badges.herokuapp.com/maven-central/de.humanfork.junit.statefullextension/junit-statefull-extension/badge.svg)](https://maven-badges.herokuapp.com/maven-central/de.humanfork.junit.statefullextension/junit-statefull-extension)

```
<dependency>
    <groupId>de.humanfork.junit.statefullextension</groupId>
    <artifactId>junit-statefull-extension</artifactId>
    <version>1.0.0</version> <!-- or newer -->
</dependency>
```

Website
-------
- https://www.humanfork.de/projects/junit-statefull-extension/ (for releases)
- https://humanfork.gitlab.io/junit-statefull-extension/index.html (for snapshots)
