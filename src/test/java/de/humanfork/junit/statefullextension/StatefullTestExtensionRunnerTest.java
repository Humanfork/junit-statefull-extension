/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.statefullextension;

import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;

import org.hamcrest.Matchers;
import org.hamcrest.core.IsInstanceOf;
import org.junit.jupiter.api.Test;

import de.humanfork.junit.statefullextension.extensions.forward.ForwardingExtension;
import de.humanfork.junit.statefullextension.extensions.forward.InstanceBaseForwardingExtension;
import de.humanfork.junit.statefullextension.extensions.resourcereuse.ResourceReuseExtension;
import de.humanfork.junit.statefullextension.extensions.resourcereuse.ResourceReusingExtension;

public class StatefullTestExtensionRunnerTest {

    /**
     * Scenario: The extensions can be defined by meta annotations.
     *
     * @throws InitializationError no exception expected
     */
    @Test
    public void testMetaAnnotation() {

        /* when: initializing StatefullTestExtensionRunner for an class that use meta annotations to define extensions */
        Junit5StatefullTestExtension runner = new Junit5StatefullTestExtension(ClassWithMetaAnnotations.class);

        /* then: this extensions will be found like they will be defined directly. */
        List<StatefullTestLifeCycle> extensions = runner.getExtensions();
        assertThat(extensions,
                Matchers.contains(IsInstanceOf.instanceOf(InstanceBaseForwardingExtension.class),
                        IsInstanceOf.instanceOf(ResourceReuseExtension.class)));
    }

    @ForwardingExtension
    @ResourceReusingExtension
    public static class ClassWithMetaAnnotations {

        @Test
        public void testDummy() {
        };
    }
}
