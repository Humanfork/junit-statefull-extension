/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.statefullextension;


import java.lang.reflect.Method;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.Sequence;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.launcher.listeners.SummaryGeneratingListener;

import de.humanfork.junit.statefullextension.integrationtestscenario.ScenarioForStatefullTestLifeCycle;
import de.humanfork.junit.util.IsListWithSize;
import de.humanfork.junit.util.Junit5Runner;
import de.humanfork.junit.util.JunitCoreCallback;
import de.humanfork.junit.util.TestMethodMatcher;

public class StatefullTestExtensionRunnerIntegrationTest {

    private Mockery context = new Mockery();

    private final StatefullTestLifeCycle statefullTestLifeCycleCallback = this.context
            .mock(StatefullTestLifeCycle.class);

    private final JunitCoreCallback junitCoreCallback = this.context.mock(JunitCoreCallback.class);

    private final Sequence sequence = this.context.sequence("lifeCycle");

    @BeforeEach
    public void injectMocksInTestClass() {
        ScenarioForStatefullTestLifeCycle.statefullTestLifeCycleCallback = this.statefullTestLifeCycleCallback;
        ScenarioForStatefullTestLifeCycle.testSequenceCallback = this.junitCoreCallback;
    }

    private Method methodTestA;

    private Method methodTestB;

    @BeforeEach
    public void setMethodValues() throws SecurityException, NoSuchMethodException {
        this.methodTestA = ScenarioForStatefullTestLifeCycle.class.getMethod("testA");
        this.methodTestB = ScenarioForStatefullTestLifeCycle.class.getMethod("testB");
    }

    /**
     * Scenario: test the life cycle on a test class that contains two test methods and a ignored third test method.
     * the expected lifecycle:
     *
     * StatefullTestLifeCycle.beforeClass
     * {@literal @} BeforeAll
     *
     * StatefullTestLifeCycle.before
     * {@literal @}Before
     * TestA
     * {@literal @}After
     * StatefullTestLifeCycle.after
     *
     * StatefullTestLifeCycle.before
     * {@literal @}Before
     * TestB
     * {@literal @}After
     * StatefullTestLifeCycle.after
     *
     * --ignored test case
     *
     * StatefullTestLifeCycle.afterClass
     * {@literal @}AfterClass
     */
    @Test
    public void testLifeCycle() {

        this.context.checking(new Expectations() {
            {
                //setup

                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.statefullTestLifeCycleCallback).beforeAll();

                inSequence(StatefullTestExtensionRunnerIntegrationTest.this.sequence);

                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.junitCoreCallback).beforeAllAnnotation();

                inSequence(StatefullTestExtensionRunnerIntegrationTest.this.sequence);

                //method A

                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.statefullTestLifeCycleCallback).beforeEach(
                        with(TestMethodMatcher.testMethodEquals(StatefullTestExtensionRunnerIntegrationTest.this.methodTestA)),
                        with(IsListWithSize.isEmpty(TestResult.class)),
                        with(any(ScenarioForStatefullTestLifeCycle.class)));

                inSequence(StatefullTestExtensionRunnerIntegrationTest.this.sequence);

                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.junitCoreCallback).beforeEachAnnotation();
                inSequence(StatefullTestExtensionRunnerIntegrationTest.this.sequence);
                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.junitCoreCallback).testA();
                inSequence(StatefullTestExtensionRunnerIntegrationTest.this.sequence);
                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.junitCoreCallback).afterEachAnnotation();

                inSequence(StatefullTestExtensionRunnerIntegrationTest.this.sequence);

                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.statefullTestLifeCycleCallback).afterEach(
                        with(IsListWithSize.hasSize(1, TestResult.class)),
                        with(any(ScenarioForStatefullTestLifeCycle.class)));

                inSequence(StatefullTestExtensionRunnerIntegrationTest.this.sequence);

                //method B

                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.statefullTestLifeCycleCallback).beforeEach(
                        with(TestMethodMatcher.testMethodEquals(StatefullTestExtensionRunnerIntegrationTest.this.methodTestB)),
                        with(IsListWithSize.hasSize(1, TestResult.class)),
                        with(any(ScenarioForStatefullTestLifeCycle.class)));

                inSequence(StatefullTestExtensionRunnerIntegrationTest.this.sequence);

                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.junitCoreCallback).beforeEachAnnotation();
                inSequence(StatefullTestExtensionRunnerIntegrationTest.this.sequence);
                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.junitCoreCallback).testB();
                inSequence(StatefullTestExtensionRunnerIntegrationTest.this.sequence);
                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.junitCoreCallback).afterEachAnnotation();

                inSequence(StatefullTestExtensionRunnerIntegrationTest.this.sequence);

                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.statefullTestLifeCycleCallback).afterEach(
                        with(IsListWithSize.hasSize(2, TestResult.class)),
                        with(any(ScenarioForStatefullTestLifeCycle.class)));

                inSequence(StatefullTestExtensionRunnerIntegrationTest.this.sequence);

                // tear down

                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.statefullTestLifeCycleCallback).afterAll();

                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.junitCoreCallback).afterAllAnnotation();

                inSequence(StatefullTestExtensionRunnerIntegrationTest.this.sequence);
            }
        });

        /* when: running the test class with two tests that both succeed */        
        SummaryGeneratingListener testResult = Junit5Runner.runTests(ScenarioForStatefullTestLifeCycle.class);
        Junit5Runner.assertFailureCount(0, testResult);

        /* then: all expected invocations must be done */
        this.context.assertIsSatisfied();
    }

    /**
     * Scenario: if an test throws an exception then the afterTestMethod is called anyway
     */
    @Test
    public void testLifeCycleWithTestException() {

        this.context.checking(new Expectations() {
            {
                //setup

                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.statefullTestLifeCycleCallback).beforeAll();

                inSequence(StatefullTestExtensionRunnerIntegrationTest.this.sequence);

                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.junitCoreCallback).beforeAllAnnotation();

                inSequence(StatefullTestExtensionRunnerIntegrationTest.this.sequence);

                //method A throws the exception

                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.statefullTestLifeCycleCallback).beforeEach(
                        with(TestMethodMatcher.testMethodEquals(StatefullTestExtensionRunnerIntegrationTest.this.methodTestA)),
                        with(IsListWithSize.isEmpty(TestResult.class)),
                        with(any(ScenarioForStatefullTestLifeCycle.class)));

                inSequence(StatefullTestExtensionRunnerIntegrationTest.this.sequence);

                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.junitCoreCallback).beforeEachAnnotation();
                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.junitCoreCallback).testA();
                will(throwException(new RuntimeException("the test failure")));
                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.junitCoreCallback).afterEachAnnotation();

                inSequence(StatefullTestExtensionRunnerIntegrationTest.this.sequence);

                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.statefullTestLifeCycleCallback).afterEach(
                        with(IsListWithSize.hasSize(1, TestResult.class)),
                        with(any(ScenarioForStatefullTestLifeCycle.class)));

                inSequence(StatefullTestExtensionRunnerIntegrationTest.this.sequence);

                //method B

                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.statefullTestLifeCycleCallback).beforeEach(
                        with(TestMethodMatcher.testMethodEquals(StatefullTestExtensionRunnerIntegrationTest.this.methodTestB)),
                        with(IsListWithSize.hasSize(1, TestResult.class)),
                        with(any(ScenarioForStatefullTestLifeCycle.class)));

                inSequence(StatefullTestExtensionRunnerIntegrationTest.this.sequence);

                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.junitCoreCallback).beforeEachAnnotation();
                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.junitCoreCallback).testB();
                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.junitCoreCallback).afterEachAnnotation();

                inSequence(StatefullTestExtensionRunnerIntegrationTest.this.sequence);

                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.statefullTestLifeCycleCallback).afterEach(
                        with(IsListWithSize.hasSize(2, TestResult.class)),
                        with(any(ScenarioForStatefullTestLifeCycle.class)));

                inSequence(StatefullTestExtensionRunnerIntegrationTest.this.sequence);

                // tear down

                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.statefullTestLifeCycleCallback).afterAll();

                oneOf(StatefullTestExtensionRunnerIntegrationTest.this.junitCoreCallback).afterAllAnnotation();

                inSequence(StatefullTestExtensionRunnerIntegrationTest.this.sequence);
            }
        });

        /* when: running the test class with two tests that both succeed */
        SummaryGeneratingListener testResult = Junit5Runner.runTests(ScenarioForStatefullTestLifeCycle.class);
        Junit5Runner.assertFailureCount(1, testResult);

        /* then: all expected invocations must be done */
        this.context.assertIsSatisfied();
    }
}
