/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.statefullextension.extension.resourcereuse.integrationtestscenario;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;

import de.humanfork.junit.statefullextension.Junit5StatefullTestExtension;
import de.humanfork.junit.statefullextension.StatefullTestExtension;
import de.humanfork.junit.statefullextension.extensions.resourcereuse.ResourceForReuse;
import de.humanfork.junit.statefullextension.extensions.resourcereuse.ResourceManager;
import de.humanfork.junit.statefullextension.extensions.resourcereuse.ResourceReuseExtension;
import de.humanfork.junit.util.JunitCoreCallback;

/**
 * Do not run this test directly, it is a scenario for an special test case!
 *
 * If you run this test directly without that test case, it will fail!
 * @author Ralph Engelmann
 */
@ExtendWith(Junit5StatefullTestExtension.class)
@StatefullTestExtension(ResourceReuseExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ScenarioWithOneResource {

    @ResourceForReuse
    public Object reusedResource;

    public static ResourceManager<Object> reusedResourceResourceManager;

    /** used to check the sequence of invocations. */
    public static JunitCoreCallback testSequenceCallback;

    @BeforeAll
    public static void beforeAllAnnotation() {
        testSequenceCallback.beforeAllAnnotation();
    }

    @AfterAll
    public static void afterAllAnnotation() {
        testSequenceCallback.afterAllAnnotation();
    }

    @BeforeEach
    public void beforeEachAnnotation() {
        testSequenceCallback.beforeEachAnnotation();
    }

    @AfterEach
    public void afterEachAnnotation() {
        testSequenceCallback.afterEachAnnotation();
    }

    @Test
    @Order(1)
    public void testA() {
        System.out.println("testA");
        testSequenceCallback.testA();
        System.out.println("testA2");
    }

    @Test
    @Order(2)
    public void testB() {
        System.out.println("testB");
        testSequenceCallback.testB();
    }
}
