/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.statefullextension.extension.resourcereuse;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.hamcrest.Matchers;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.junit5.JUnit5Mockery;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.extension.ExtendWith;

import de.humanfork.junit.statefullextension.TestResult;
import de.humanfork.junit.statefullextension.extensions.resourcereuse.DirtysResource;
import de.humanfork.junit.statefullextension.extensions.resourcereuse.RequiresNewResource;
import de.humanfork.junit.statefullextension.extensions.resourcereuse.ResourceConnector;
import de.humanfork.junit.statefullextension.extensions.resourcereuse.ResourceForReuse;
import de.humanfork.junit.statefullextension.extensions.resourcereuse.ResourceManager;
import de.humanfork.junit.statefullextension.extensions.resourcereuse.ResourceReuseExtension;
import de.humanfork.junit.util.DummyResourceManager;
import de.humanfork.junit.util.FakeResourceConnector;
import de.humanfork.junit.util.TestMethodMatcher;

/**
 * The Class ResourceReuseExtensionTest.
 */
@ExtendWith(JUnit5Mockery.class)
public class ResourceReuseExtensionTest {

    /** managed by jmock */
    private Mockery context = new Mockery();

    /**
     * scenario: if no resource manager is specified for the resource, than the
     * resource manager is take from an static field that has the same name like the
     * resource field but with an extra postfix "ResourceManager".
     */
    @Test
    public void testResourceManagerWithResourceFromStaticField() {

        /*
         * given: the sample class ResourceManagerByStaticField with the dummy resource
         * manager
         */
        ResourceManager<Object> fileResourceManager = ResourceManagerByStaticField.fileResourceManager;

        /** when: instantiating the resource reuse extension */
        ResourceReuseExtension resourceReuseExtension = new ResourceReuseExtension(ResourceManagerByStaticField.class);

        /**
         * then: the ResourceReuseExtension must have a connection between the file
         * field and the fileResourceManger
         */
        assertThat(resourceReuseExtension.getResourceConnectors(), Matchers.hasSize(1));
        ResourceConnector resourceConnector = resourceReuseExtension.getResourceConnectors().get(0);
        assertSame(fileResourceManager, resourceConnector.getResourceManager());
        assertEquals("file", resourceConnector.getResourceField().getName());
    }

    /**
     * Demo class for test cases.
     *
     * @author Ralph Engelmann
     *
     */
    public static class ResourceManagerByStaticField {

        /** The file. */
        @ResourceForReuse
        public Object file;

        /** The file resource manager. */
        public static ResourceManager<Object> fileResourceManager = new DummyResourceManager();
    }

    /**
     * scenario: the class of the resource manager can be specified the
     * {@link ResourceForReuse} annotation.
     */
    @Test
    public void testResourceManagerWithResourceByAttributeResourceManagerClass() {

        /** when: instantiating the resource reuse extension */
        ResourceReuseExtension resourceReuseExtension = new ResourceReuseExtension(
                ResourceManagerByAnnotationAttributeResourceManagerClass.class);

        /**
         * then: the ResourceReuseExtension must have a connection between the file
         * field an instance of DummyResourceManager
         */
        assertThat(resourceReuseExtension.getResourceConnectors(), Matchers.hasSize(1));
        ResourceConnector resourceConnector = resourceReuseExtension.getResourceConnectors().get(0);
        assertSame(DummyResourceManager.class, resourceConnector.getResourceManager().getClass());
        assertEquals("file", resourceConnector.getResourceField().getName());
    }

    /**
     * Demo class for test cases.
     *
     * @author Ralph Engelmann
     *
     */
    public static class ResourceManagerByAnnotationAttributeResourceManagerClass {

        /** The file. */
        @ResourceForReuse(resourceManagerClass = DummyResourceManager.class)
        public Object file;

    }

    /**
     * scenario: If the resource manager specified the {@link ResourceForReuse} has
     * an constructor with only one parameter of type class, then this constructor
     * will be used. The Parameter value will be the test class.
     */
    @Test
    public void testResourceManagerWithResourceByAttributeResourceManagerClassWithConstructorParameter() {

        /** when: instantiating the resource reuse extension */
        ResourceReuseExtension resourceReuseExtension = new ResourceReuseExtension(
                ResourceManagerByAnnotationAttributeResourceManagerClassWithClassConstructor.class);

        /** then: the Resource manager must have know the test class */
        assertThat(resourceReuseExtension.getResourceConnectors(), Matchers.hasSize(1));
        ResourceConnector resourceConnector = resourceReuseExtension.getResourceConnectors().get(0);
        assertEquals(ResourceManagerByAnnotationAttributeResourceManagerClassWithClassConstructor.class,
                ((ClassConstructorDummyResourceManager) resourceConnector.getResourceManager())
                        .getConstructorParameter());
    }

    /**
     * Demo class for test cases.
     *
     * @author Ralph Engelmann
     *
     */
    public static class ResourceManagerByAnnotationAttributeResourceManagerClassWithClassConstructor {

        /** The file. */
        @ResourceForReuse(resourceManagerClass = ClassConstructorDummyResourceManager.class)
        public Object file;

    }

    /**
     * The Class ClassConstructorDummyResourceManager.
     */
    public static class ClassConstructorDummyResourceManager extends DummyResourceManager {

        private final Class<?> constructorParameter;

        public ClassConstructorDummyResourceManager(final Class<?> constructorParameter) {
            this.constructorParameter = constructorParameter;
        }

        public Class<?> getConstructorParameter() {
            return this.constructorParameter;
        }
    }

    private static class FakeTestInfo implements TestInfo {

        private Method testMethod;

        public FakeTestInfo(final Method testMethod) {
            this.testMethod = testMethod;
        }

        @Override
        public String getDisplayName() {
            return this.testMethod.getName();
        }

        @Override
        public Set<String> getTags() {
            return Collections.emptySet();
        }

        @Override
        public Optional<Class<?>> getTestClass() {
            return Optional.of(this.testMethod.getDeclaringClass());
        }

        @Override
        public Optional<Method> getTestMethod() {
            return Optional.of(this.testMethod);
        }

    }

    /**
     * scenario: if an method has a {@link RequiresNewResource} annotation, then the value get injected by the
     * resource manager.
     *
     * @throws SecurityException     the security exception
     * @throws NoSuchFieldException  the no such field exception
     * @throws NoSuchMethodException the no such method exception
     *                               {@link ResourceManager#reinitialize(Object, FrameworkMethod, List, Object)}
     *                               method will be invoked
     */
    @Test
    public void testBeforeEachWithRequireNewResource()
            throws SecurityException, NoSuchFieldException, NoSuchMethodException {

        /* given: a resource manager (Mock) that is used by an resource extension via the resource connector */
        final Object oldResource = new Object();
        final Object newResource = new Object();

        @SuppressWarnings("unchecked")
        final ResourceManager<Object> resourceManager = this.context.mock(ResourceManager.class);
        FakeResourceConnector resourceConnector = new FakeResourceConnector(resourceManager);
        resourceConnector.setSavedResource(oldResource);

        ResourceReuseExtension extension = new ResourceReuseExtension(
                Arrays.<ResourceConnector> asList(resourceConnector));

        final Method anyMethod = getAnyMethod();
        final List<TestResult> testHistory = Arrays.asList(new TestResult(new FakeTestInfo(anyMethod)));
        final Method requiresNewResourceTestMethod = getRequiresNewResourceTestMethod();

        final Container requiresNewResourceContainer = new Container();

        /* then: the initNew method will be used instead of reuse after success */
        this.context.checking(new Expectations() {
            {
                oneOf(resourceManager).reinitialize(with(oldResource),
                        with(TestMethodMatcher.testMethodEquals(requiresNewResourceTestMethod)),
                        with(testHistory),
                        with(requiresNewResourceContainer));
                will(returnValue(newResource));
            }
        });

        /* when: simulating a new test method invocation and simulates that the last test was successful */
        extension
                .beforeEach(new FakeTestInfo(requiresNewResourceTestMethod), testHistory, requiresNewResourceContainer);

        /* then: the resource has its new value */
        assertEquals(newResource, resourceConnector.getResource(requiresNewResourceContainer));
        assertEquals(newResource, resourceConnector.getSavedResource());
    }

    /**
     * scenario: if an method has a {@link DirtysResource} annotation, then the next
     * test method will not use the resource again, instead the
     * {@link ResourceManager#reinitialize(Object, FrameworkMethod, List, Object)}
     * will be invoked.
     *
     * @throws SecurityException     the security exception
     * @throws NoSuchFieldException  the no such field exception
     * @throws NoSuchMethodException the no such method exception
     */
    @Test
    public void testBeforeEachWithDirtysResource()
            throws SecurityException, NoSuchFieldException, NoSuchMethodException {

        /* given: a resource manager (Mock) that is used by an resource extension via the resource connector */
        final Object oldResource = new Object();
        final Object newResource = new Object();

        @SuppressWarnings("unchecked")
        final ResourceManager<Object> resourceManager = this.context.mock(ResourceManager.class);
        FakeResourceConnector resourceConnector = new FakeResourceConnector(resourceManager);
        resourceConnector.setSavedResource(oldResource);

        ResourceReuseExtension extension = new ResourceReuseExtension(
                Arrays.<ResourceConnector> asList(resourceConnector));

        final Method anyMethod = getAnyMethod();
        final Method dirtysResourceTestMethod = getDirtysResourceTestMethod();

        final List<TestResult> testHistory = Arrays.asList(new TestResult(new FakeTestInfo(dirtysResourceTestMethod)));

        final Container anyMethodTestContainer = new Container();

        /** then: the initNew method will be used instead of reuse after success */
        this.context.checking(new Expectations() {
            {
                oneOf(resourceManager).reinitialize(with(oldResource),
                        with(TestMethodMatcher.testMethodEquals(anyMethod)),
                        with(testHistory),
                        with(anyMethodTestContainer));
                will(returnValue(newResource));
            }
        });

        /* when: simulating a new test method invocation and simulates that the previous one has the DirtyResource
         * Annotation */
        extension.beforeEach(new FakeTestInfo(anyMethod), testHistory, anyMethodTestContainer);

        /** then: the resource has its new value */
        assertEquals(newResource, resourceConnector.getResource(anyMethodTestContainer));
        assertEquals(newResource, resourceConnector.getSavedResource());
    }

    /**
     * The Class Container.
     */
    public static class Container {

        /**
         * Any method.
         */
        public void anyMethod() {
        }

        /**
         * Requires new resource test method.
         */
        @RequiresNewResource
        public void requiresNewResourceTestMethod() {
        }

        /**
         * Dirtys resource test method.
         */
        @DirtysResource
        public void dirtysResourceTestMethod() {
        }
    }

    private Method getAnyMethod() throws NoSuchMethodException {
        return Container.class.getDeclaredMethod("anyMethod");
    }

    private Method getRequiresNewResourceTestMethod() throws NoSuchMethodException {
        return Container.class.getDeclaredMethod("requiresNewResourceTestMethod");
    }

    private Method getDirtysResourceTestMethod() throws NoSuchMethodException {
        return Container.class.getDeclaredMethod("dirtysResourceTestMethod");
    }

    /**
     * scenario: the Annotation {@link ResourceForReuse} can be used as meta
     * annotation
     */
    @Test
    public void testResourceManagerWithResourceMetaAnnotation() {

        /** when: instantiating the resource reuse extension */
        ResourceReuseExtension resourceReuseExtension = new ResourceReuseExtension(
                ResourceManagerByMetaAnnotation.class);

        /**
         * then: the ResourceReuseExtension must have a connection between the file
         * field an instance of DummyResourceManager
         */
        assertThat(resourceReuseExtension.getResourceConnectors(), Matchers.hasSize(1));
        ResourceConnector resourceConnector = resourceReuseExtension.getResourceConnectors().get(0);
        assertSame(DummyResourceManager.class, resourceConnector.getResourceManager().getClass());
        assertEquals("file", resourceConnector.getResourceField().getName());
    }

    /**
     * Demo class for test cases.
     *
     * @author Ralph Engelmann
     *
     */
    public static class ResourceManagerByMetaAnnotation {

        /** The file. */
        @DummyResource
        public Object file;
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.FIELD, ElementType.ANNOTATION_TYPE })
    @ResourceForReuse(resourceManagerClass = DummyResourceManager.class)
    public @interface DummyResource {

    }
}
