/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.statefullextension.extension.resourcereuse;


import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.Sequence;
import org.jmock.junit5.JUnit5Mockery;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.launcher.listeners.SummaryGeneratingListener;

import de.humanfork.junit.statefullextension.TestResult;
import de.humanfork.junit.statefullextension.extension.resourcereuse.integrationtestscenario.ScenarioWithOneResource;
import de.humanfork.junit.statefullextension.extensions.resourcereuse.ResourceManager;
import de.humanfork.junit.util.IsListWithSize;
import de.humanfork.junit.util.Junit5Runner;
import de.humanfork.junit.util.JunitCoreCallback;

/**
 * Test the lifecycle
 * the general lifecycle:
 * <ol>
 *    <li>{@literal @}BeforeClass                                </li>
 *    <li>ResourceLifeCycle.init                                 </li>
 *
 *    <li>{@literal @}Before                                     </li>
 *    <li>TestA                                                  </li>
 *    <li>{@literal @}After                                      </li>
 *
 *    <li>ResourceLifeCycle.reuseAfterSuccess                    </li>
 *
 *    <li>{@literal @}Before                                     </li>
 *    <li>TestB                                                  </li>
 *    <li>{@literal @}After                                      </li>
 *
 *    <li>ResourceLifeCycle.cleanup                              </li>
 *
 *    <li>{@literal @}AfterClass                                 </li>
 * </ol>
 */
@ExtendWith(JUnit5Mockery.class)
public class ResourceReuseExtensionIntegrationTest {

    private Mockery context = new Mockery();

    @SuppressWarnings("unchecked")
    private final ResourceManager<Object> resourceLifeCycleMock = this.context.mock(ResourceManager.class);

    private final JunitCoreCallback testSequenceCallback = this.context.mock(JunitCoreCallback.class);

    private final Sequence sequence = this.context.sequence("lifeCycle");

    /** scenario: check the life cycle for an test class with two succeeding test cases. */
    @Test
    public void testLifeCycleWithSuccess() {
        /*
         * given: the ScenarioWithOneResource test class with an mocked reusedResourceResourceLifeCycle
         */

        ScenarioWithOneResource.reusedResourceResourceManager = this.resourceLifeCycleMock;
        ScenarioWithOneResource.testSequenceCallback = this.testSequenceCallback;

        final Object resourceAfterInit = new Object();
        final Object resourceAfterReuse = new Object();

        this.context.checking(new Expectations() {
            {
                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).beforeAllAnnotation();

                inSequence(ResourceReuseExtensionIntegrationTest.this.sequence);

                oneOf(ResourceReuseExtensionIntegrationTest.this.resourceLifeCycleMock)
                        .init(with(any(TestInfo.class)), with(any(ScenarioWithOneResource.class)));
                will(returnValue(resourceAfterInit));

                inSequence(ResourceReuseExtensionIntegrationTest.this.sequence);

                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).beforeEachAnnotation();
                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).testA();
                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).afterEachAnnotation();

                inSequence(ResourceReuseExtensionIntegrationTest.this.sequence);

                oneOf(ResourceReuseExtensionIntegrationTest.this.resourceLifeCycleMock).reuseAfterSuccess(
                        with(equal(resourceAfterInit)),
                        with(any(TestInfo.class)),
                        with(IsListWithSize.hasSize(1, TestResult.class)),
                        with(any(ScenarioWithOneResource.class)));
                will(returnValue(resourceAfterReuse));

                inSequence(ResourceReuseExtensionIntegrationTest.this.sequence);

                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).beforeEachAnnotation();
                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).testB();
                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).afterEachAnnotation();

                inSequence(ResourceReuseExtensionIntegrationTest.this.sequence);

                oneOf(ResourceReuseExtensionIntegrationTest.this.resourceLifeCycleMock).cleanup(resourceAfterReuse);

                inSequence(ResourceReuseExtensionIntegrationTest.this.sequence);

                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).afterAllAnnotation();

                inSequence(ResourceReuseExtensionIntegrationTest.this.sequence);
            }
        });

        /* when: running the test class with two tests that both succeed */
        SummaryGeneratingListener testResult = Junit5Runner.runTests(ScenarioWithOneResource.class);
        Junit5Runner.assertFailureCount(1, testResult);

        /* then: all expected invocations must be done */
    }

    /**
     * scenario: when an exception is thrown and there is one more test,
     * then the reuseAfterFail method is invoked before the next test
     */
    @Test
    public void testLifeCycleWithFailuere() {
        /*
         * given: the ScenarioWithOneResource test class with an mocked reusedResourceResourceLifeCycle
         */

        ScenarioWithOneResource.reusedResourceResourceManager = this.resourceLifeCycleMock;
        ScenarioWithOneResource.testSequenceCallback = this.testSequenceCallback;

        final Object resourceAfterInit = new Object();
        final Object resourceAfterReuse = new Object();

        this.context.checking(new Expectations() {
            {
                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).beforeAllAnnotation();

                inSequence(ResourceReuseExtensionIntegrationTest.this.sequence);

                oneOf(ResourceReuseExtensionIntegrationTest.this.resourceLifeCycleMock)
                        .init(with(any(TestInfo.class)), with(any(ScenarioWithOneResource.class)));
                will(returnValue(resourceAfterInit));

                inSequence(ResourceReuseExtensionIntegrationTest.this.sequence);

                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).beforeEachAnnotation();
                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).testA();
                will(throwException(new RuntimeException("Test Exception: simulate test failure")));
                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).afterEachAnnotation();

                inSequence(ResourceReuseExtensionIntegrationTest.this.sequence);

                oneOf(ResourceReuseExtensionIntegrationTest.this.resourceLifeCycleMock).reuseAfterFail(
                        with(equal(resourceAfterInit)),
                        with(any(TestInfo.class)),
                        with(IsListWithSize.hasSize(1, TestResult.class)),
                        with(any(ScenarioWithOneResource.class)));
                will(returnValue(resourceAfterReuse));

                inSequence(ResourceReuseExtensionIntegrationTest.this.sequence);

                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).beforeEachAnnotation();
                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).testB();
                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).afterEachAnnotation();

                inSequence(ResourceReuseExtensionIntegrationTest.this.sequence);

                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).afterAllAnnotation();

                inSequence(ResourceReuseExtensionIntegrationTest.this.sequence);

                oneOf(ResourceReuseExtensionIntegrationTest.this.resourceLifeCycleMock).cleanup(resourceAfterReuse);

                inSequence(ResourceReuseExtensionIntegrationTest.this.sequence);
            }
        });

        /* when: running the test class with where one of two tests succeed */
        SummaryGeneratingListener testResult = Junit5Runner.runTests(ScenarioWithOneResource.class);
        this.context.assertIsSatisfied();
        Junit5Runner.assertFailureCount(1, testResult);
        
        /* then: all expected invocations must be done */
    }

    /** scenario: check the life cycle for an test class where the last test is failing. */
    @Test
    public void testLifeCycleWithFailingLastTest() {
        /*
         * given: the ScenarioWithOneResource test class with an mocked reusedResourceResourceLifeCycle
         */

        ScenarioWithOneResource.reusedResourceResourceManager = this.resourceLifeCycleMock;
        ScenarioWithOneResource.testSequenceCallback = this.testSequenceCallback;

        final Object resourceAfterInit = new Object();
        final Object resourceAfterReuse = new Object();

        this.context.checking(new Expectations() {
            {
                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).beforeAllAnnotation();

                inSequence(ResourceReuseExtensionIntegrationTest.this.sequence);

                oneOf(ResourceReuseExtensionIntegrationTest.this.resourceLifeCycleMock)
                        .init(with(any(TestInfo.class)), with(any(ScenarioWithOneResource.class)));
                will(returnValue(resourceAfterInit));

                inSequence(ResourceReuseExtensionIntegrationTest.this.sequence);

                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).beforeEachAnnotation();
                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).testA();
                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).afterEachAnnotation();

                inSequence(ResourceReuseExtensionIntegrationTest.this.sequence);

                oneOf(ResourceReuseExtensionIntegrationTest.this.resourceLifeCycleMock).reuseAfterSuccess(
                        with(equal(resourceAfterInit)),
                        with(any(TestInfo.class)),
                        with(IsListWithSize.hasSize(1, TestResult.class)),
                        with(any(ScenarioWithOneResource.class)));
                will(returnValue(resourceAfterReuse));

                inSequence(ResourceReuseExtensionIntegrationTest.this.sequence);

                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).beforeEachAnnotation();
                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).testB();
                will(throwException(new RuntimeException("Test Exception: simulate test failure")));
                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).afterEachAnnotation();

                inSequence(ResourceReuseExtensionIntegrationTest.this.sequence);

                oneOf(ResourceReuseExtensionIntegrationTest.this.testSequenceCallback).afterAllAnnotation();

                inSequence(ResourceReuseExtensionIntegrationTest.this.sequence);

                oneOf(ResourceReuseExtensionIntegrationTest.this.resourceLifeCycleMock).cleanup(resourceAfterReuse);

                inSequence(ResourceReuseExtensionIntegrationTest.this.sequence);
            }
        });

        /* when: running the test class with where one of two tests succeed */
        SummaryGeneratingListener testResult = Junit5Runner.runTests(ScenarioWithOneResource.class);
        this.context.assertIsSatisfied();
        Junit5Runner.assertFailureCount(1, testResult);

        /* then: all expected invocations must be done */
    }

}
