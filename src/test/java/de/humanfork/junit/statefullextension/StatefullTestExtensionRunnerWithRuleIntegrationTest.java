/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.statefullextension;

import java.lang.reflect.Method;
import java.util.List;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.Sequence;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.launcher.listeners.SummaryGeneratingListener;

import de.humanfork.junit.statefullextension.integrationtestscenario.ScenarioForStatefullTestLifeCycleWithRule;
import de.humanfork.junit.util.IsListWithSize;
import de.humanfork.junit.util.Junit5Runner;
import de.humanfork.junit.util.JunitCoreCallback;
import de.humanfork.junit.util.TestMethodMatcher;

public class StatefullTestExtensionRunnerWithRuleIntegrationTest {

    private Mockery context = new Mockery();

    private final StatefullTestLifeCycle statefullTestLifeCycleCallback = this.context
            .mock(StatefullTestLifeCycle.class);

    private final JunitCoreCallback junitCoreCallback = this.context.mock(JunitCoreCallback.class);

    private final Sequence sequence = this.context.sequence("lifeCycle");

    @BeforeEach
    public void injectMocksInTestClass() {
        ScenarioForStatefullTestLifeCycleWithRule.statefullTestLifeCycleCallback = this.statefullTestLifeCycleCallback;
        ScenarioForStatefullTestLifeCycleWithRule.testSequenceCallback = this.junitCoreCallback;
    }

    private Method methodTestA;

    private Method methodTestB;

    private Method methodTestC;

    @BeforeEach
    public void setMethodValues() throws SecurityException, NoSuchMethodException {
        this.methodTestA = ScenarioForStatefullTestLifeCycleWithRule.class.getMethod("testA");
        this.methodTestB = ScenarioForStatefullTestLifeCycleWithRule.class.getMethod("testB");
        this.methodTestC = ScenarioForStatefullTestLifeCycleWithRule.class.getMethod("testC");
    }

    /**
     * Scenario: test the life cycle wiht a specific method rule that repeats a
     * failed test once. The test class that contains tree test methods. the first
     * (A) always succeed, the second (B) fails the first invocation, but passed the
     * second one the third (C) always fails. the expected lifecycle:
     *
     * {@literal @}BeforeClass StatefullTestLifeCycle.beforeClass
     *
     * StatefullTestLifeCycle.before {@literal @}Before TestA {@literal @}After
     * StatefullTestLifeCycle.after
     *
     * StatefullTestLifeCycle.before {@literal @}Before TestB (fail)
     * {@literal @}After StatefullTestLifeCycle.after StatefullTestLifeCycle.before
     * {@literal @}Before TestB {@literal @}After StatefullTestLifeCycle.after
     *
     *
     * StatefullTestLifeCycle.before {@literal @}Before TestC (fail)
     * {@literal @}After StatefullTestLifeCycle.after StatefullTestLifeCycle.before
     * {@literal @}Before TestC (fail) {@literal @}After
     * StatefullTestLifeCycle.after
     *
     * StatefullTestLifeCycle.afterClass {@literal @}AfterClass
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testLifeCycle() {

        this.context.checking(new Expectations() {
            {
                // setup

                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.statefullTestLifeCycleCallback)
                        .beforeAll();

                inSequence(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.sequence);

                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.junitCoreCallback).beforeAllAnnotation();

                inSequence(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.sequence);

                // method A

                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.statefullTestLifeCycleCallback)
                        .beforeEach(
                                with(TestMethodMatcher.testMethodEquals(
                                        StatefullTestExtensionRunnerWithRuleIntegrationTest.this.methodTestA)),
                                with(IsListWithSize.isEmpty(TestResult.class)),
                                with(any(ScenarioForStatefullTestLifeCycleWithRule.class)));

                inSequence(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.sequence);

                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.junitCoreCallback)
                        .beforeEachAnnotation();
                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.junitCoreCallback).testA();
                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.junitCoreCallback).afterEachAnnotation();

                inSequence(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.sequence);

                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.statefullTestLifeCycleCallback)
                        .afterEach(with(IsListWithSize.hasSize(1, TestResult.class)),
                                with(any(ScenarioForStatefullTestLifeCycleWithRule.class)));

                inSequence(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.sequence);

                // method B

                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.statefullTestLifeCycleCallback)
                        .beforeEach(
                                with(TestMethodMatcher.testMethodEquals(
                                        StatefullTestExtensionRunnerWithRuleIntegrationTest.this.methodTestB)),
                                with(IsListWithSize.hasSize(1, TestResult.class)),
                                with(any(ScenarioForStatefullTestLifeCycleWithRule.class)));

                inSequence(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.sequence);

                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.junitCoreCallback)
                        .beforeEachAnnotation();
                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.junitCoreCallback).testB();
                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.junitCoreCallback).afterEachAnnotation();

                inSequence(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.sequence);

                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.statefullTestLifeCycleCallback)
                        .afterEach(with(IsListWithSize.hasSize(2, TestResult.class)),
                                with(any(ScenarioForStatefullTestLifeCycleWithRule.class)));

                inSequence(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.sequence);

                // method B second try

                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.statefullTestLifeCycleCallback)
                        .beforeEach(
                                with(TestMethodMatcher.testMethodEquals(
                                        StatefullTestExtensionRunnerWithRuleIntegrationTest.this.methodTestB)),
                                with(IsListWithSize.hasSize(2, TestResult.class)),
                                with(any(ScenarioForStatefullTestLifeCycleWithRule.class)));

                inSequence(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.sequence);

                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.junitCoreCallback)
                        .beforeEachAnnotation();
                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.junitCoreCallback).testB();
                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.junitCoreCallback).afterEachAnnotation();

                inSequence(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.sequence);

                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.statefullTestLifeCycleCallback)
                        .afterEach(with(IsListWithSize.hasSize(3, TestResult.class)), // ???
                                with(any(ScenarioForStatefullTestLifeCycleWithRule.class)));

                inSequence(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.sequence);

                // method C

                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.statefullTestLifeCycleCallback)
                        .beforeEach(
                                with(TestMethodMatcher.testMethodEquals(
                                        StatefullTestExtensionRunnerWithRuleIntegrationTest.this.methodTestC)),
                                (List<TestResult>) with(IsListWithSize.<TestResult> hasSize(3)),
                                with(any(ScenarioForStatefullTestLifeCycleWithRule.class)));

                inSequence(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.sequence);

                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.junitCoreCallback)
                        .beforeEachAnnotation();
                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.junitCoreCallback).testC();
                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.junitCoreCallback).afterEachAnnotation();

                inSequence(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.sequence);

                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.statefullTestLifeCycleCallback)
                        .afterEach(with(IsListWithSize.hasSize(4, TestResult.class)),
                                with(any(ScenarioForStatefullTestLifeCycleWithRule.class)));

                inSequence(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.sequence);

                // method C second try

                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.statefullTestLifeCycleCallback)
                        .beforeEach(
                                with(TestMethodMatcher.testMethodEquals(
                                        StatefullTestExtensionRunnerWithRuleIntegrationTest.this.methodTestC)),
                                with(IsListWithSize.hasSize(4, TestResult.class)),
                                with(any(ScenarioForStatefullTestLifeCycleWithRule.class)));

                inSequence(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.sequence);

                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.junitCoreCallback)
                        .beforeEachAnnotation();
                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.junitCoreCallback).testC();
                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.junitCoreCallback).afterEachAnnotation();

                inSequence(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.sequence);

                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.statefullTestLifeCycleCallback)
                        .afterEach(with(IsListWithSize.hasSize(5, TestResult.class)),
                                with(any(ScenarioForStatefullTestLifeCycleWithRule.class)));

                inSequence(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.sequence);

                // tear down

                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.statefullTestLifeCycleCallback)
                        .afterAll();

                oneOf(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.junitCoreCallback).afterAllAnnotation();

                inSequence(StatefullTestExtensionRunnerWithRuleIntegrationTest.this.sequence);
            }
        });

        /*
         * when: running the test class with three tests,
         * but test "b" and "c" fail at the first time and succed at the second try
         */

        SummaryGeneratingListener result = Junit5Runner.runTests(ScenarioForStatefullTestLifeCycleWithRule.class);

        this.context.assertIsSatisfied();

        Junit5Runner.assertFailureCount(2, result);

        /* then: all expected invocations must be done */
        this.context.assertIsSatisfied();
    }
}
