/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.statefullextension.integrationtestscenario;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;

import de.humanfork.junit.statefullextension.Junit5StatefullTestExtension;
import de.humanfork.junit.statefullextension.StatefullTestExtension;
import de.humanfork.junit.statefullextension.StatefullTestLifeCycle;
import de.humanfork.junit.statefullextension.extensions.forward.Forward;
import de.humanfork.junit.statefullextension.extensions.forward.InstanceBaseForwardingExtension;
import de.humanfork.junit.util.JunitCoreCallback;
import io.github.artsok.RepeatedIfExceptionsTest;

/**
 * Do not run this test directly, it is a scenario for an special test case!
 *
 * If you run this test directly without that test case, it will fail!
 *
 * The special point of this scenario is, that it has Rule which repeats the test
 * exactly once if it has an exception in the first run.
 *
 * @author Ralph Engelmann
 */
@ExtendWith(Junit5StatefullTestExtension.class)
@StatefullTestExtension(InstanceBaseForwardingExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ScenarioForStatefullTestLifeCycleWithRule {

    /**
     * Used to check the sequence of invocations.
     *
     * Field must be populated by the test case, before the test on this class starts.
     */
    public static JunitCoreCallback testSequenceCallback;

    /**
     * Used to check the sequence of invocations.
     *
     * Field must be populated by the test case, before the test on this class starts.
     */
    @Forward
    public static StatefullTestLifeCycle statefullTestLifeCycleCallback;

    @BeforeAll
    public static void beforeAll() {
        System.out.println("run beforeAll");
        if (testSequenceCallback != null) {
            testSequenceCallback.beforeAllAnnotation();
        } else {
            System.out.println("warning: no testSequenceCallback instanciated");
        }
    }

    @AfterAll
    public static void afterAll() {
        System.out.println("run afterAll");
        if (testSequenceCallback != null) {
            testSequenceCallback.afterAllAnnotation();
        } else {
            System.out.println("warning: no testSequenceCallback instanciated");
        }
    }

    @BeforeEach
    public void beforeEach() {
        System.out.println("run beforeEach");
        if (testSequenceCallback != null) {
            testSequenceCallback.beforeEachAnnotation();
        } else {
            System.out.println("warning: no testSequenceCallback instanciated");
        }
    }

    @AfterEach
    public void afterEach() {
        System.out.println("run afterEach");
        if (testSequenceCallback != null) {
            testSequenceCallback.afterEachAnnotation();
        } else {
            System.out.println("warning: no testSequenceCallback instanciated");
        }
    }

    @Test
    @Order(1)
    public void testA() {
        System.out.println("run afterClass");
        if (testSequenceCallback != null) {
            testSequenceCallback.testA();
        } else {
            System.out.println("warning: no testSequenceCallback instanciated");
        }
    }

    private static int TEST_B_COUNTER = 0;

    @Test
    @RepeatedIfExceptionsTest
    @Order(2)
    public void testB() {
        if (testSequenceCallback != null) {
            testSequenceCallback.testB();
        } else {
            System.out.println("warning: no testSequenceCallback instanciated");
        }

        TEST_B_COUNTER++;
        if (TEST_B_COUNTER == 1) {
            System.out.println("run testB -- and fail");
            throw new RuntimeException("fail the first test");
        } else {
            System.out.println("run testB -- and pass");
        }
    }

    private static int TEST_C_COUNTER = 0;
    
    @Test
    @Order(3)
    @RepeatedIfExceptionsTest
    public void testC() throws SpecialException {
        System.out.println("testC -- and fail");
        if (testSequenceCallback != null) {
            testSequenceCallback.testC();
        } else {
            System.out.println("no testSequenceCallback instanciated");
        }
        
        TEST_C_COUNTER++;
        if (TEST_C_COUNTER == 1) {
            System.out.println("run testC -- and fail");
            throw new SpecialException("fail the first test");
        } else {
            System.out.println("run testC -- and pass");
        }
    }

    /**
     * Only a exception to test the exception handling.
     * @author Ralph Engelmann
     *
     */
    public static class SpecialException extends Exception {
        /** Serial version UID. */
        private static final long serialVersionUID = -4324445056958875934L;

        public SpecialException(final String message) {
            super(message);
        }
    }
}
