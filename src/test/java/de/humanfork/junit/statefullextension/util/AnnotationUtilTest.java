/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.statefullextension.util;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.hamcrest.MatcherAssert.assertThat;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;

import javax.enterprise.util.AnnotationLiteral;

import org.junit.jupiter.api.Test;

import de.humanfork.junit.util.IsListWithSize;

/** Test the {@link AnnotationUtil} class. */
public class AnnotationUtilTest {

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.METHOD, ElementType.TYPE, ElementType.FIELD })
    public @interface Meta {
        String value();
    }

    private static final String META_VALUE_OF_SUB = "Meta Of Sub";

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.METHOD, ElementType.TYPE, ElementType.FIELD })
    @Meta(META_VALUE_OF_SUB)
    public @interface Sub {
    }

    private static final String META_VALUE_OF_OTHER_SUB = "Meta Of Other Sub";

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.METHOD, ElementType.TYPE, ElementType.FIELD })
    @Meta(META_VALUE_OF_OTHER_SUB)
    private @interface OtherSub {
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.METHOD, ElementType.TYPE, ElementType.FIELD })
    @Sub
    @OtherSub
    private @interface SubSub {
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.METHOD, ElementType.TYPE, ElementType.FIELD })
    @Recursive("inner")
    private @interface Recursive {
        String value();
    }

    @Recursive("outer")
    private @interface RecursiveContext {
    }

    private static class MetaClass extends AnnotationLiteral<Meta> implements Meta {

        private static final long serialVersionUID = -674664484502732094L;

        private String value;

        public MetaClass(final String value) {
            this.value = value;
        }

        @Override
        public String value() {
            return this.value;
        }
    };

    private static class SubClass extends AnnotationLiteral<Sub> implements Sub {

        private static final long serialVersionUID = -360016617853651975L;
    };

    private static class SubSubClass extends AnnotationLiteral<SubSub> implements SubSub {

        private static final long serialVersionUID = 3744767342275277780L;
    };

    private static class RecursiveClass extends AnnotationLiteral<Recursive> implements Recursive {

        private static final long serialVersionUID = 1436890960110684349L;

        private String value;

        public RecursiveClass(final String value) {
            this.value = value;
        }

        @Override
        public String value() {
            return this.value;
        }
    };

    private static class RecursiveContextClass extends AnnotationLiteral<RecursiveContext> implements RecursiveContext {

        private static final long serialVersionUID = 3271951927233314694L;
    };

    @Test
    public void TestFindOneMetaAnnotationWithDirectMatch() {
        assertEquals(new MetaClass("test"), AnnotationUtil.findOneMetaAnnotation(Meta.class, new MetaClass("test")));
    }

    @Test
    public void TestFindOneMetaAnnotationWithSubClass() {
        assertEquals(new MetaClass(META_VALUE_OF_SUB),
                AnnotationUtil.findOneMetaAnnotation(Meta.class, new SubClass()));
    }

    @Test
    public void TestFindAllMetaAnnotationWithDirectMatch() {
        List<Meta> result = AnnotationUtil.findAllMetaAnnotation(Meta.class, new MetaClass("test"));
        assertThat(result, IsListWithSize.hasSize(1));
        assertEquals(new MetaClass("test"), result.get(0));
    }

    @Test
    public void TestFindAllMetaAnnotationWithSubClass() {
        List<Meta> result = AnnotationUtil.findAllMetaAnnotation(Meta.class, new SubClass());
        assertThat(result, IsListWithSize.hasSize(1));
        assertEquals(new MetaClass(META_VALUE_OF_SUB), result.get(0));
    }

    @Test
    public void TestFindAllMetaAnnotationWithTwoMeta() {
        List<Meta> result = AnnotationUtil.findAllMetaAnnotation(Meta.class, new SubSubClass());
        assertThat(result, IsListWithSize.hasSize(2));
        assertEquals(new MetaClass(META_VALUE_OF_SUB), result.get(0));
        assertEquals(new MetaClass(META_VALUE_OF_OTHER_SUB), result.get(1));
    }

    @Test
    public void TestFindAllMetaAnnotationWithRecursiveAnnotation() {
        List<Recursive> result = AnnotationUtil.findAllMetaAnnotation(Recursive.class, new RecursiveContextClass());
        assertThat(result, IsListWithSize.hasSize(2));
        assertEquals(new RecursiveClass("outer"), result.get(0));
        assertEquals(new RecursiveClass("inner"), result.get(1));
    }
}
