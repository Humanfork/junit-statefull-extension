/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.util;

/**
 * Callback of the standard junit live cycle.
 * @author Ralph Engelmann
 *
 */
public interface JunitCoreCallback {

    /**
     * Before all tests.
     */
    void beforeAllAnnotation();

    /**
     * After all tests.
     */
    void afterAllAnnotation();

    /**
     * Before each test.
     */
    void beforeEachAnnotation();

    /**
     * After each test.
     */
    void afterEachAnnotation();

    /**
     * Test a.
     */
    void testA();

    /**
     * Test b.
     */
    void testB();

    /**
     * Test c.
     */
    void testC();
}
