/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.util;

import java.util.List;

import org.junit.jupiter.api.TestInfo;

import de.humanfork.junit.statefullextension.TestResult;
import de.humanfork.junit.statefullextension.extensions.resourcereuse.ResourceManager;

/**
 * A ResourceManager that always return null.
 */
public class DummyResourceManager implements ResourceManager<Object> {

    /* (non-Javadoc)
     * @see de.humanfork.junit.statefullextension.extensions.resourcereuse.ResourceManager#init(org.junit.runners.model.FrameworkMethod, java.lang.Object)
     */
    @Override
    public Object init(final TestInfo nextTestMethod, final Object testClass) {
        return null;
    }

    /* (non-Javadoc)
     * @see de.humanfork.junit.statefullextension.extensions.resourcereuse.ResourceManager#reuseAfterSuccess(java.lang.Object, org.junit.runners.model.FrameworkMethod, java.util.List, java.lang.Object)
     */
    @Override
    public Object reuseAfterSuccess(final Object currentState, final TestInfo nextTestMethod,
            final List<TestResult> testHistory, final Object testClass) {
        return null;
    }

    /* (non-Javadoc)
     * @see de.humanfork.junit.statefullextension.extensions.resourcereuse.ResourceManager#reuseAfterFail(java.lang.Object, org.junit.runners.model.FrameworkMethod, java.util.List, java.lang.Object)
     */
    @Override
    public Object reuseAfterFail(final Object currentState, final TestInfo nextTestMethod,
            final List<TestResult> testHistory, final Object testClass) {
        return null;
    }

    /* (non-Javadoc)
     * @see de.humanfork.junit.statefullextension.extensions.resourcereuse.ResourceManager#cleanup(java.lang.Object)
     */
    @Override
    public Object cleanup(final Object currentState) {
        return null;
    }

    /* (non-Javadoc)
     * @see de.humanfork.junit.statefullextension.extensions.resourcereuse.ResourceManager#reinitialize(java.lang.Object, org.junit.runners.model.FrameworkMethod, java.util.List, java.lang.Object)
     */
    @Override
    public Object reinitialize(final Object currentState, final TestInfo nextTestMethod,
            final List<TestResult> testHistory, final Object testClass) {
        return null;
    }

}
