/*
 * Copyright 2019-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.util;

import java.util.List;

import org.junit.jupiter.api.TestInfo;

import de.humanfork.junit.statefullextension.StatefullTestLifeCycle;
import de.humanfork.junit.statefullextension.TestResult;

/**
 * Print every invocation to system.out.
 * @author Ralph Engelmann
 *
 */
public class LoggingStatefullTestLifeCycle implements StatefullTestLifeCycle {

    @Override
    public void beforeAll() {
        System.out.println("LoggingStatefullTestLifeCycle - beforeAll");
    }

    @Override
    public void afterAll() {
        System.out.println("LoggingStatefullTestLifeCycle - afterAll");
    }

    @Override
    public void beforeEach(final TestInfo nextTestMethod, final List<TestResult> testHistory,
            final Object testClass) {
        System.out.println("LoggingStatefullTestLifeCycle - beforeEach");
    }

    @Override
    public void afterEach(final List<TestResult> testHistory, final Object testClass) {
        System.out.println("LoggingStatefullTestLifeCycle - afterEach");
    }

}
