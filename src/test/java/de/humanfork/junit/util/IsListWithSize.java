/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.util;

import static org.hamcrest.core.IsEqual.equalTo;

import java.util.List;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;

/**
 * Matches if List size satisfies a nested matcher.
 *
 * @param <E> the list element type
 */
public class IsListWithSize<E> extends TypeSafeMatcher<List<? extends E>> {

    /** The matcher used to check if the size is correct. */
    private Matcher<? super Integer> sizeMatcher;

    /**
     * Instantiates a new checks if is list with size.
     *
     * @param sizeMatcher the size matcher
     */
    public IsListWithSize(final Matcher<? super Integer> sizeMatcher) {
        super();
        this.sizeMatcher = sizeMatcher;
    }

    /**
     * Does List size satisfy a given matcher?.
     *
     * @param <E> the element type
     * @param size the size
     * @return the matcher
     */    
    public static <E> Matcher<List<? extends E>> hasSize(final Matcher<? super Integer> size) {
        return new IsListWithSize<E>(size);
    }

    /**
     * Checks for size.
     *
     * @param <E> the element type
     * @param size the size
     * @return the matcher
     */
    public static <E> Matcher<List<? extends E>> hasSize(final int size) {
        Matcher<? super Integer> matcher = equalTo(size);
        return IsListWithSize.<E> hasSize(matcher);
    }
    
    /**
     * Checks for size.
     *
     * @param <E> the element type
     * @param size the size
     * @return the matcher
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static <E> Matcher<List<E>> hasSize(final int size, Class<E> type) {        
        Matcher matcher = IsListWithSize.<E> hasSize(equalTo(size));
        return (Matcher<List<E>>)  matcher;
    }
    
    /**
     * Checks that the collection is empty
     *
     * @param <E> the element type
     * @param size the size
     * @return the matcher
     */
    public static <E> Matcher<List<E>> isEmpty(Class<E> type) {        
        return hasSize(0, type);
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static <E> Matcher<List<E>> hasOneMatchingItem(final Matcher<E> firstItemMatcher, Class<E> type) {
        Matcher hasItem = Matchers.hasItem(firstItemMatcher);
        return Matchers.both(hasSize(1)).and(hasItem);
    }

    /* (non-Javadoc)
     * @see org.hamcrest.SelfDescribing#describeTo(org.hamcrest.Description)
     */
    @Override
    public void describeTo(final Description description) {
        description.appendText("List of size");
        this.sizeMatcher.describeTo(description);
    }

    /* (non-Javadoc)
     * @see org.hamcrest.TypeSafeMatcher#matchesSafely(java.lang.Object)
     */
    @Override
    public boolean matchesSafely(final List<? extends E> list) {
        return this.sizeMatcher.matches(Integer.valueOf(list.size()));
    }

}
