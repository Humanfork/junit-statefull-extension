/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.util;

import java.lang.reflect.Method;
import java.util.Optional;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import de.humanfork.junit.statefullextension.TestResult;

/**
 * Hamcrest Matcher to check that an JUnit Framework Method matches an expected
 * Java Reflection Method.
 * 
 * @author engelmann
 *
 */
public class TestMatcherMatcherForResult extends TypeSafeDiagnosingMatcher<TestResult> {

    private final Method expectedMethod;

    public TestMatcherMatcherForResult(final Method expectedMethod) {
        this.expectedMethod = expectedMethod;
    }

	public static TestMatcherMatcherForResult testMethodEquals(Method expectedMethod) {
		return new TestMatcherMatcherForResult(expectedMethod);
	}

    @Override
    public void describeTo(final Description description) {
        description.appendText("has test method(")
                .appendValue(expectedMethod != null ? expectedMethod.getName() : "null").appendText(")");
    }

    @Override
    protected boolean matchesSafely(final TestResult item, final Description mismatchDescription) {
        // Object.equals

        if (this.expectedMethod != null) {
            return item.getMethod().getTestMethod().equals(Optional.ofNullable(this.expectedMethod));
        } else {
            return item == null;
        }
    }

}
