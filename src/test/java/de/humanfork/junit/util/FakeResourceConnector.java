/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.util;

import java.util.HashMap;
import java.util.Map;

import de.humanfork.junit.statefullextension.extensions.resourcereuse.ResourceConnector;
import de.humanfork.junit.statefullextension.extensions.resourcereuse.ResourceManager;

/**
 * Simulate an Resource Connector.
 * @author Ralph Engelmann
 *
 */
public class FakeResourceConnector extends ResourceConnector {

    /**
     * Simulates the different resources in different test objects.
     * key = test object
     * value = resource in that object
     */
    private Map<Object, Object> fakeResources = new HashMap<Object, Object>();

    /** This field will never be used, but the super class need a field for its constructor. */
    public Object fakeResourceField;

    /**
     * Instantiates a new fake resource connector.
     *
     * @param resourceLifeCycle the resource life cycle
     * @throws SecurityException the security exception
     * @throws NoSuchFieldException the no such field exception
     */
    public FakeResourceConnector(final ResourceManager<Object> resourceLifeCycle)
            throws SecurityException, NoSuchFieldException {
        super(FakeResourceConnector.class.getField("fakeResourceField"), resourceLifeCycle);
    }

    /**
     * Gets the fake resources.
     *
     * @return the fake resources
     */
    public Map<Object, Object> getFakeResources() {
        return this.fakeResources;
    }

    @Override
    public Object getResource(final Object target) {
        return this.fakeResources.get(target);
    }

    @Override
    public void setResource(final Object target, final Object value) {
        this.fakeResources.put(target, value);
    }

    @Override
    public Object getSavedResource() {
        return super.getSavedResource();
    }

    @Override
    public void setSavedResource(final Object savedResource) {
        super.setSavedResource(savedResource);
    }

}
