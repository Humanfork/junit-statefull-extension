/*
 * Copyright 2019-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Collectors;

import org.junit.platform.engine.discovery.DiscoverySelectors;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.junit.platform.launcher.listeners.SummaryGeneratingListener;

public class Junit5Runner {

    public static SummaryGeneratingListener runTests(Class<?> classUnderTest) {

        SummaryGeneratingListener listener = new SummaryGeneratingListener();

        LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request()
                .selectors(DiscoverySelectors.selectClass(classUnderTest)).build();
        Launcher launcher = LauncherFactory.create();
        //        TestPlan testPlan = launcher.discover(request);
        launcher.registerTestExecutionListeners(listener);
        launcher.execute(request);

        return listener;
    }

    public static void assertFailureCount(int expectedFailureCount, SummaryGeneratingListener actualTestResult) {
        assertEquals(expectedFailureCount,
                actualTestResult.getSummary().getFailures().size(),
                actualTestResult.getSummary().getFailures().stream().map(Junit5Runner::failureToString)
                        .collect(Collectors.joining()));
    }

    private static String failureToString(
            final org.junit.platform.launcher.listeners.TestExecutionSummary.Failure failure) {

        return "name: " + failure.getTestIdentifier().getDisplayName() + "\n failute:"
                + failure.getException().getClass() + " : " + failure.getException().getMessage() + "\n";
    }

}
