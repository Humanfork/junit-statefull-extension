/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.statefullextension;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import de.humanfork.junit.statefullextension.util.AnnotationUtil;

/**
 * Junit Runner that allows to add statefull reusable extension to an test
 * class. The extensions are indicated by {@link StatefullTestExtension}
 * annotation.
 * 
 * TestInstancePostProcessor for injection TestWatcher to access the test result
 *
 * TODO Use ExtensionContext.store TODO Support Nested Test Classes TODO Use
 * ExtensionContext instead of TestInfo?
 *
 * @author Ralph Engelmann
 *
 */
public class Junit5StatefullTestExtension implements BeforeAllCallback, BeforeEachCallback, AfterTestExecutionCallback,
		AfterEachCallback, AfterAllCallback {

	/**
	 * The extensions supported by this runner.
	 */
	private List<StatefullTestLifeCycle> extensions;

	/**
	 * The result of the previous done tests. The first test is the first item in
	 * this history. A test item will been added to this history, right after the
	 * test is done, but before the
	 * {@link StatefullTestLifeCycle#afterEach(List, Object)} are executed.
	 *
	 * <p>
	 * This history can also be used to determine whether the current test is the
	 * first executed test, then the history would be empty.
	 * <p>
	 */
	private LinkedList<TestResult> testHistorie = new LinkedList<TestResult>();

	/**
	 * Constructor with lazy initialization.
	 */
	public Junit5StatefullTestExtension() {
		this.extensions = null;
	}

	/**
	 * Constructor with eager initialization.
	 * 
	 * @param clazz
	 */
	public Junit5StatefullTestExtension(Class<?> testClass) {
		this.extensions = initExtensions(testClass);
	}

	public List<StatefullTestLifeCycle> getExtensions() {
		return extensions;
	}

	public LinkedList<TestResult> getTestHistorie() {
		return testHistorie;
	}

	/**
	 * Instantiates a the extensions and call there before Class methods.
	 *
	 * @param context the current extension context
	 */
	@Override
	public void beforeAll(ExtensionContext extensionContext) {
		if (this.extensions == null) {
			Class<?> testClass = extensionContext.getRequiredTestClass();
			this.extensions = initExtensions(testClass);
		}

		this.extensions.forEach(StatefullTestLifeCycle::beforeAll);
	}

	@Override
	public void beforeEach(ExtensionContext extensionContext) {
		// context.getRequiredTestInstances();
		Object testClass = extensionContext.getRequiredTestInstance();
		StatefullTestInfo nextTestMethod = new StatefullTestInfo(extensionContext);

		this.extensions.forEach(extension -> extension.beforeEach(nextTestMethod, this.testHistorie, testClass));
	}

	@Override
	public void afterTestExecution(ExtensionContext extensionContext) {
		this.testHistorie.add(new TestResult(new StatefullTestInfo(extensionContext),
				extensionContext.getExecutionException().orElse(null)));
	}

	@Override
	public void afterEach(ExtensionContext extensionContext) {
		Object testClass = extensionContext.getRequiredTestInstance();

		this.extensions.forEach(extension -> extension.afterEach(this.testHistorie, testClass));
	}

	@Override
	public void afterAll(ExtensionContext context) {
		this.extensions.forEach(StatefullTestLifeCycle::afterAll);
	}

	/**
	 * Inits the extensions.
	 *
	 * @param clazz the test class
	 * @return the initialized extensions defined in this test class
	 */
	private List<StatefullTestLifeCycle> initExtensions(final Class<?> clazz) {
		return instantiateExtensions(scanForExtensions(clazz), clazz);
	}

	/**
	 * Scan for extensions in the test class
	 *
	 * @param clazz the test class
	 * @return the classes of extensions
	 */
	private List<Class<? extends StatefullTestLifeCycle>> scanForExtensions(final Class<?> clazz) {
		Class<?> superClass = clazz.getSuperclass();
		final List<Class<? extends StatefullTestLifeCycle>> result;
		if (superClass != null) {
			result = scanForExtensions(superClass);
		} else {
			result = new ArrayList<Class<? extends StatefullTestLifeCycle>>();
		}

		List<StatefullTestExtension> findAllMetaAnnotation = AnnotationUtil
				.findAllMetaAnnotation(StatefullTestExtension.class, clazz.getAnnotations());
		for (StatefullTestExtension statefullTestExtension : findAllMetaAnnotation) {
			result.addAll(Arrays.asList(statefullTestExtension.value()));
		}

		return result;
	}

	/**
	 * Instantiate the extensions.
	 *
	 * @param extensionClasses the extension classes
	 * @param testClass        the test class
	 * @return the instanciated extensions
	 */
	private List<StatefullTestLifeCycle> instantiateExtensions(
			final List<Class<? extends StatefullTestLifeCycle>> extensionClasses, final Class<?> testClass) {
		List<StatefullTestLifeCycle> result = new ArrayList<StatefullTestLifeCycle>(extensionClasses.size());
		for (Class<? extends StatefullTestLifeCycle> extensionClass : extensionClasses) {
			result.add(instantiateExtension(extensionClass, testClass));
		}
		return result;
	}

	/**
	 * Instantiate a extension.
	 *
	 * @param extensionClass the extension class
	 * @param testClass      the test class
	 * @return the extensions
	 */
	private StatefullTestLifeCycle instantiateExtension(final Class<? extends StatefullTestLifeCycle> extensionClass,
			final Class<?> testClass) {
		try {
			Constructor<? extends StatefullTestLifeCycle> constructor = extensionClass.getConstructor(Class.class);
			return constructor.newInstance(testClass);
		} catch (IllegalArgumentException e) {
			throw new RuntimeException("Could not instanciate extension `" + extensionClass + "`", e);
		} catch (InstantiationException e) {
			throw new RuntimeException("Could not instanciate extension `" + extensionClass + "`", e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException("Could not instanciate extension `" + extensionClass + "`", e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException("Could not instanciate extension `" + extensionClass + "`", e);
		} catch (SecurityException e) {
			throw new RuntimeException("Could not instanciate extension `" + extensionClass + "`", e);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException("Could not instanciate extension `" + extensionClass + "`", e);
		}
	}

	public static class StatefullTestInfo implements TestInfo {

		private final String displayName;
		private final Set<String> tags;
		private final Optional<Class<?>> testClass;
		private final Optional<Method> testMethod;

		StatefullTestInfo(ExtensionContext extensionContext) {
			this.displayName = extensionContext.getDisplayName();
			this.tags = extensionContext.getTags();
			this.testClass = extensionContext.getTestClass();
			this.testMethod = extensionContext.getTestMethod();
		}

		@Override
		public String getDisplayName() {
			return this.displayName;
		}

		@Override
		public Set<String> getTags() {
			return this.tags;
		}

		@Override
		public Optional<Class<?>> getTestClass() {
			return this.testClass;
		}

		@Override
		public Optional<Method> getTestMethod() {
			return this.testMethod;
		}

		@Override
		public String toString() {
			return "StatefullTestInfo [displayName=" + displayName + ", tags=" + tags + ", testClass=" + testClass
					+ ", testMethod=" + testMethod + "]";
		}

	}
}
