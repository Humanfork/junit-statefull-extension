/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
/**
 * JUnit extension that provides a platform to build reusable statefull extension.
 * <ul>
 *   <li>
 *     Reusable means that you will not need to write all the code in the test classes, so you can use it in different test
 *     classes.
 *   </li>
 *   <li>
 *     Statefull means that the extension instances will be created once for every test class instead of every test case.
 *   </li>
 * </ul>
 *
 * The extensions must implement the {@link de.humanfork.junit.statefullextension.StatefullTestLifeCycle} interface, that
 * contains some methods which will be invoked by the runner.
 */
package de.humanfork.junit.statefullextension;
