/*
 * Copyright 2018-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.statefullextension.extensions.resourcereuse;

import java.util.List;

import de.humanfork.junit.statefullextension.TestResult;

/**
 * A {@link ResourceManager} with an additional method that gets invoked right after every test,
 * to inform about the current test result.
 *
 * @author Ralph Engelmann
 *
 * @param <T> the type of the managed resource
 */
public interface ResourceManagerWithTestResultObserver<T> extends ResourceManager<T> {

    /**
     * This Method is invoked after every test case.
     * @param currentState the resource after the n-th test
     * @param testHistory the test history, contains the test methods and the results
     * @param testClass the test class
     */
    void afterTest(T currentState, List<TestResult> testHistory, Object testClass);

}
