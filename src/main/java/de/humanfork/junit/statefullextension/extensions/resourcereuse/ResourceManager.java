/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.statefullextension.extensions.resourcereuse;

import java.util.List;
import org.junit.jupiter.api.TestInfo;
import de.humanfork.junit.statefullextension.TestResult;

/**
 * Implementations of this interface are responsible to manage one resource for all tests that are done within one class.
 *
 * An instance of this class will be created before the first test of the class is stared, and will live as long the tests
 * of this class run.
 *
 * @author Ralph Engelmann
 *
 * @param <T> the type of the managed resource
 */
public interface ResourceManager<T> {

    /**
     * This method must provide an instance of the managed resource so that the first test can be done.
     * Will be invoked before the first test is run.
     *
     * @param nextTestMethod the next test method
     * @param testClass the test class
     * @return the resource ready to use
     */
    T init(final TestInfo nextTestMethod, Object testClass);

    /**
     * This method must provide an instance of the managed resource so that the next test can be done.
     * Will be invoked before the (n+1)-th test is started, but only if the n-th test was successfully.
     *
     * @param currentState the resource after the n-th test
     * @param nextTestMethod the next test method
     * @param testHistory the test history
     * @param testClass the test class
     * @return the resource ready to use
     */
    T reuseAfterSuccess(T currentState, TestInfo nextTestMethod, List<TestResult> testHistory, Object testClass);

    /**
     * This method must provide an instance of the managed resource so that the next test can be done.
     * Will be invoked before the (n+1)-th test is started, but only if the n-th test was not successfully.
     *
     * @param currentState the resource after the n-th test
     * @param nextTestMethod the next test method
     * @param testHistory the test history
     * @param testClass the test class
     * @return the resource ready to use
     */
    T reuseAfterFail(T currentState, TestInfo nextTestMethod, List<TestResult> testHistory, Object testClass);

    /**
     * This method must cleanup the resource.
     * It will be invoked after the last test
     * @param currentState the resource after the n-th test
     * @return the cleaned resource -- normally {@code null}.
     */
    T cleanup(T currentState);

    /**
     * This method must cleanup and initialize a new resource.
     * It will be invoked if the test case requires an new resource.
     *
     * In many cases the implementation can call {@link #cleanup(Object)} and {@link #init(FrameworkMethod, Object)}.
     *
     * @param currentState the current state
     * @param nextTestMethod the next test method
     * @param testHistory the test history
     * @param testClass the test class
     * @return the new initialized resource.
     * @see RequiresNewResource
     */
    T reinitialize(T currentState, TestInfo nextTestMethod, List<TestResult> testHistory, Object testClass);

}
