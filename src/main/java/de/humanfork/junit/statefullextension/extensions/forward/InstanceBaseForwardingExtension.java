/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.statefullextension.extensions.forward;

/**
 * Extension that pay's respect to the {@link Forward} annotation.
 *
 * @see de.humanfork.junit.statefullextension.extensions.forward package java doc
 * @author Ralph Engelmann
 */
public class InstanceBaseForwardingExtension extends AbstractInstanceBaseForwardingExtension<Forward> {

    /**
     * Instantiates a new instance base forwarding extension.
     *
     * @param clazz the clazz
     */
    public InstanceBaseForwardingExtension(final Class<?> clazz) {
        super(clazz, Forward.class);
    }

}
