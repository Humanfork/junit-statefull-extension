/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
/**
 * Statefull extension to forward the life cycle invocations to classes defined by static fields in the test class.
 *
 * The test class must be annotated with:
 * <pre><code>
 * {@literal @}RunWith(StatefullTestExtensionRunner.class) //to enable the Statefull Runner
 * {@literal @}tatefullTestExtension(InstanceBaseForwardingExtension.class) //to enable this extension
 * </code></pre>
 *
 * Then the test class must have a static variable implementing a subclass of
 * {@link de.humanfork.junit.statefullextension.StatefullTestLifeCycle}
 * which is annotated by @Forward
 * <pre><code>
 *  {@literal @}Forward
 *  public static StatefullTestLifeCycle statefullTestLifeCycleCallback = new CustomStatefullTestLifeCycle;
 * </code></pre>
 *
 * Then this callback is invoked according to the test cycle.
 */
package de.humanfork.junit.statefullextension.extensions.forward;
