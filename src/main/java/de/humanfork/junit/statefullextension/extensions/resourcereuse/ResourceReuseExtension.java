/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.statefullextension.extensions.resourcereuse;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.TestInfo;
import org.junit.platform.commons.support.AnnotationSupport;

import de.humanfork.junit.statefullextension.StatefullTestLifeCycle;
import de.humanfork.junit.statefullextension.TestResult;
import de.humanfork.junit.statefullextension.util.AnnotationUtil;

/**
 * Extension to reuse resource (marked with {@link ResourceForReuse} in different tests.
 * 
 * @author Ralph Engelmann
 */
public class ResourceReuseExtension implements StatefullTestLifeCycle {

    /** The Connectors that connect a concrete resource with an resource manager. */
    private final List<ResourceConnector> resourceConnectors;

    /**
     * Instantiates a new resource reuse extension.
     *
     * @param clazz the clazz
     * @throws ResourceReuseException the resource reuse exception
     */
    public ResourceReuseExtension(final Class<?> clazz) throws ResourceReuseException {
        this.resourceConnectors = scanForResources(clazz);
    }

    /**
     * Instantiates a new resource reuse extension.
     *
     * @param resourceConnectors  The Connectors that connect a concrete resource with an resource manager
     * @throws ResourceReuseException the resource reuse exception
     */
    public ResourceReuseExtension(final List<ResourceConnector> resourceConnectors) throws ResourceReuseException {
        this.resourceConnectors = resourceConnectors;
    }

    /**
     * Gets the resource connectors.
     *
     * @return the resource connectors
     */
    public List<ResourceConnector> getResourceConnectors() {
        return this.resourceConnectors;
    }

    @Override
    public void beforeAll() {
        //nothing to do
    }

    @Override
    public void afterAll() {
        for (ResourceConnector resourceLifeCycleState : this.resourceConnectors) {
            resourceLifeCycleState.clenup();
        }
    }

    /**
     * Invoke the different resource handling and before methods for all resource connectors.
     *
     * @param nextTestMethod the next test method
     * @param testHistory the test history
     * @param testClass the test class
     */
    @Override
    public void beforeEach(final TestInfo nextTestMethod, final List<TestResult> testHistory,
            final Object testClass) {

        for (ResourceConnector resourceConnector : this.resourceConnectors) {
            resourceConnector.beforeEachTestBeforeResourceHandling(nextTestMethod, testHistory, testClass);
        }

        if (testHistory.isEmpty()) {
            for (ResourceConnector resourceConnector : this.resourceConnectors) {
                resourceConnector.beforeFirstTest(nextTestMethod, testClass);
            }
        } else {
        	

        	
        	
            TestResult lastTest = testHistory.get(testHistory.size() - 1);
            if (AnnotationSupport.isAnnotated(nextTestMethod.getTestMethod().get(), RequiresNewResource.class)
                    || AnnotationSupport.isAnnotated(lastTest.getMethod().getTestMethod().get(), DirtysResource.class)) {
                for (ResourceConnector resourceLifeCycleState : this.resourceConnectors) {
                    resourceLifeCycleState.reinitialize(nextTestMethod, testHistory, testClass);
                }
            } else {
                if (lastTest.isFailure()) {
                    for (ResourceConnector resourceLifeCycleState : this.resourceConnectors) {
                        resourceLifeCycleState.reuseAfterFail(nextTestMethod, testHistory, testClass);
                    }
                } else {
                    for (ResourceConnector resourceLifeCycleState : this.resourceConnectors) {
                        resourceLifeCycleState.reuseAfterSuccess(nextTestMethod, testHistory, testClass);
                    }
                }
            }
        }

        for (ResourceConnector resourceConnector : this.resourceConnectors) {
            resourceConnector.beforeEachTestAfterResourceHandling(nextTestMethod, testHistory, testClass);
        }
    }

    @Override
    public void afterEach(final List<TestResult> testHistorie, final Object testClass) {
        for (ResourceConnector resourceConnector : this.resourceConnectors) {
            resourceConnector.saveResource(testHistorie, testClass);
            resourceConnector.afterTest(testHistorie, testClass);
        }
    }

    /**
     * Scan for resources that should be managed
     *
     * @param clazz the test class
     * @return the resource connectors for that resources
     * @throws ResourceReuseException the resource reuse exception
     */
    private List<ResourceConnector> scanForResources(final Class<?> clazz) throws ResourceReuseException {
        // @formatter:off
        return AnnotationUtil.scanForFields(clazz)
                .filter(AnnotationUtil.hasMetaAnnotation(ResourceForReuse.class))
                .map((resourceField) -> buildResourceConnector(resourceField, clazz))
                .collect(Collectors.toList());
        // @formatter:on
    }

    /**
     * Create a resource connector.
     *
     * @param resourceField the resource field
     * @param clazz the test class
     * @return the resource connector
     * @throws ResourceReuseException the resource reuse exception
     */
    private ResourceConnector buildResourceConnector(final Field resourceField, final Class<?> clazz)
            throws ResourceReuseException {

        final ResourceManager<Object> resourceManger;
        ResourceForReuse resourceAnnotation = AnnotationUtil.findOneMetaAnnotation(ResourceForReuse.class,
                resourceField.getAnnotations());
        if (((resourceAnnotation.resourceManagerClass() != null)
                && !resourceAnnotation.resourceManagerClass().equals(ResourceForReuse.NoResouceManager.class))) {
            resourceManger = useResourceManagerFromAnnotationAttributeResourceManagerClass(resourceAnnotation, clazz);
        } else {
            resourceManger = useResourceManagerFromStaticField(resourceField, clazz);
        }

        return new ResourceConnector(resourceField, resourceManger);
    }

    /**
     * Create an new resource manager specified by annotation attribute resource manager class.
     * If the resource manager has a constructor with only one parameter of type class, then this constructor will be
     * used to create an instance of the {@link ResourceManager}.
     * Its parameter will be the test class.
     * If this constructor does not exist, the constructor without parameter will be used.
     *
     * @param resourceAnnotation the resource annotation
     * @param clazz the test class
     * @return the resource manager
     */
    private ResourceManager<Object> useResourceManagerFromAnnotationAttributeResourceManagerClass(
            final ResourceForReuse resourceAnnotation, final Class<?> clazz) {
        Class<? extends ResourceManager<?>> resourceManagerClass = resourceAnnotation.resourceManagerClass();

        try {

            try {
                @SuppressWarnings("unchecked")
                ResourceManager<Object> resourceManager = (ResourceManager<Object>) resourceManagerClass
                        .getConstructor(Class.class).newInstance(clazz);
                return resourceManager;
            } catch (NoSuchMethodException e) {
                @SuppressWarnings("unchecked")
                ResourceManager<Object> resourceManager = (ResourceManager<Object>) resourceManagerClass
                        .getConstructor().newInstance();
                return resourceManager;
            }
        } catch (IllegalArgumentException e) {
            throw new ResourceReuseException("could not instantiate resouce manager: " + resourceManagerClass.getName(),
                    e);
        } catch (SecurityException e) {
            throw new ResourceReuseException("could not instantiate resouce manager: " + resourceManagerClass.getName(),
                    e);
        } catch (InstantiationException e) {
            throw new ResourceReuseException("could not instantiate resouce manager: " + resourceManagerClass.getName(),
                    e);
        } catch (IllegalAccessException e) {
            throw new ResourceReuseException("could not instantiate resouce manager: " + resourceManagerClass.getName(),
                    e);
        } catch (InvocationTargetException e) {
            throw new ResourceReuseException("could not instantiate resouce manager: " + resourceManagerClass.getName(),
                    e);
        } catch (NoSuchMethodException e) {
            throw new ResourceReuseException("could not instantiate resouce manager: " + resourceManagerClass.getName(),
                    e);
        }
    }

    /**
     * Load the resource manager form an static field.
     * The field name ins the name of the resource field with "ResourceManager" postfix.
     *
     * @param resourceField the resource field
     * @param clazz the clazz
     * @return the resource manager
     */
    @SuppressWarnings("unchecked")
    private ResourceManager<Object> useResourceManagerFromStaticField(final Field resourceField, final Class<?> clazz) {
        final String expectedResourceManagerFieldName = resourceField.getName() + "ResourceManager";
        Field resourceManagerField = AnnotationUtil.findField(clazz, expectedResourceManagerFieldName)
                .orElseThrow(() -> new ResourceReuseException(
                        "There is no resource manager for the resource in field " + resourceField.getName()
                                + " (expected a field with name `" + expectedResourceManagerFieldName + "`) "));

        ResourceManager<Object> resourceManger;
        try {
            resourceManger = (ResourceManager<Object>) resourceManagerField.get(clazz);
        } catch (IllegalAccessException e) {
            throw new IllegalStateException(
                    "Error while obtaining resourceManager for " + resourceField + " from resourseManagerField "
                            + resourceManagerField,
                    e);
        }
        if (resourceManger == null) {
            throw new ResourceReuseException("the resource manager `" + resourceManagerField + "` must not be null");
        }
        return resourceManger;
    }
}
