/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.statefullextension.extensions.resourcereuse;

import java.lang.reflect.Field;
import java.util.List;

import org.junit.jupiter.api.TestInfo;

import de.humanfork.junit.statefullextension.TestResult;

/** Make a resource statefull and connect it with its {@link ResourceManager}. */
public class ResourceConnector {

    /** The field in the test class that contains the managed resource. */
    private Field resourceField;

    /** The resource value. */
    private Object savedResource;

    /** Manager to manager the resource. */
    private ResourceManager<Object> resourceManager;

    /**
     * Instantiates a new resource connector.
     *
     * @param resourceField the resource field
     * @param resourceManager the resource manager
     */
    public ResourceConnector(final Field resourceField, final ResourceManager<Object> resourceManager) {
        this.resourceField = resourceField;
        this.resourceManager = resourceManager;
    }

    /**
     * Gets the resource field.
     *
     * @return the resource field
     */
    public Field getResourceField() {
        return this.resourceField;
    }

    /**
     * Gets the resource manager.
     *
     * @return the resource manager
     */
    public ResourceManager<Object> getResourceManager() {
        return this.resourceManager;
    }

    /**
     * Gets the resource.
     *
     * @param target the target
     * @return the resource
     */
    protected Object getResource(final Object target) {
        try {
            return this.resourceField.get(target);
        } catch (IllegalAccessException ex) {
            throw new IllegalStateException(
                    "Unexpected reflection exception while get value from field: " + this.resourceField,
                    ex);
        }
    }

    /**
     * Sets the resource.
     *
     * @param target the target
     * @param value the value
     */
    protected void setResource(final Object target, final Object value) {
        try {
            this.resourceField.set(target, value);
        } catch (IllegalAccessException ex) {
            throw new IllegalStateException(
                    "Unexpected reflection exception while set value to field: " + this.resourceField,
                    ex);
        }
    }

    /**
     * Gets the saved resource.
     *
     * @return the saved resource
     */
    protected Object getSavedResource() {
        return this.savedResource;
    }

    /**
     * Sets the saved resource.
     *
     * @param savedResource the new saved resource
     */
    protected void setSavedResource(final Object savedResource) {
        this.savedResource = savedResource;
    }

    /**
     * Will be invoked once before the first test is done.
     * Must initialize the resource.
     *
     * @param nextTestMethod the next test method
     * @param testClass the test class
     */
    public void beforeFirstTest(final TestInfo nextTestMethod, final Object testClass) {
        Object resource = this.resourceManager.init(nextTestMethod, testClass);
        setResource(testClass, resource);

        /* save the resource for the case that there is not test case and so the saveResource method is not called */
        this.savedResource = resource;
    }

    /**
     * Will be invoked after the last test.
     * Must clenup the resource.
     */
    public void clenup() {
        this.savedResource = this.resourceManager.cleanup(this.savedResource);
    }

    /**
     * Reuse resource after success test.
     *
     * @param nextTestMethod the next test method
     * @param testHistory the test history
     * @param testClass the test class
     */
    public void reuseAfterSuccess(final TestInfo nextTestMethod, final List<TestResult> testHistory,
            final Object testClass) {
        this.savedResource = this.resourceManager
                .reuseAfterSuccess(this.savedResource, nextTestMethod, testHistory, testClass);
        setResource(testClass, this.savedResource);
    }

    /**
     * Reuse resource after test fail.
     *
     * @param nextTestMethod the next test method
     * @param testHistory the test history
     * @param testClass the test class
     */
    public void reuseAfterFail(final TestInfo nextTestMethod, final List<TestResult> testHistory,
            final Object testClass) {
        this.savedResource = this.resourceManager
                .reuseAfterFail(this.savedResource, nextTestMethod, testHistory, testClass);
        setResource(testClass, this.savedResource);
    }

    /**
     * Save resource.
     *
     * @param testHistorie the test history - contains the executed methods and results - the last item was the last
     * executed one
     * @param testClass the test class
     */
    public void saveResource(final List<TestResult> testHistorie, final Object testClass) {
        this.savedResource = getResource(testClass);
    }

    /**
     * Reinitialize the resource because of an {@link RequiresNewResource} of {@link DirtysResource} annotation.
     *
     * @param nextTestMethod the next test method
     * @param testHistory the test history
     * @param testClass the test class
     */
    public void reinitialize(final TestInfo nextTestMethod, final List<TestResult> testHistory,
            final Object testClass) {
        this.savedResource = this.resourceManager
                .reinitialize(this.savedResource, nextTestMethod, testHistory, testClass);
        setResource(testClass, this.savedResource);
    }

    /**
     * Inform the {@link #resourceManager} about the begin of an test case.
     *
     * When the {@link #resourceManager} implements {@link ResouceManagerWithTestResultObserver} then it should be
     * informed about the test case begin.
     *
     * @param nextTestMethod the next test method
     * @param testHistorie the test history - contains the executed methods and results - the last item was the last
     * executed one
     * @param testClass the test class
     */
    public void beforeEachTestBeforeResourceHandling(final TestInfo nextTestMethod, final List<TestResult> testHistorie,
            final Object testClass) {
        if (this.resourceManager instanceof ResourceManagerWithBeforeTestObserver) {

            //unchecked is valid, because the savedResouce is delivered by the ResourceManager itself
            @SuppressWarnings({ "unchecked", "rawtypes" })
            ResourceManagerWithBeforeTestObserver<Object> observer = (ResourceManagerWithBeforeTestObserver) this.resourceManager;
            observer.beforeEachTestBeforeResourceHandling(this.savedResource, nextTestMethod, testHistorie, testClass);
        }
    }

    /**
     * Inform the {@link #resourceManager} about the begin of an test case and its initialized resources.
     *
     * When the {@link #resourceManager} implements {@link ResouceManagerWithTestResultObserver} then it should be
     * informed about the test case begin.
     *
     * @param nextTestMethod the next test method
     * @param testHistorie the test history - contains the executed methods and results - the last item was the last
     * executed one
     * @param testClass the test class
     */
    public void beforeEachTestAfterResourceHandling(final TestInfo nextTestMethod,
            final List<TestResult> testHistorie, final Object testClass) {
        if (this.resourceManager instanceof ResourceManagerWithBeforeTestObserver) {

            //unchecked is valid, because the savedResouce is delivered by the ResourceManager itself
            @SuppressWarnings({ "unchecked", "rawtypes" })
            ResourceManagerWithBeforeTestObserver<Object> observer = (ResourceManagerWithBeforeTestObserver) this.resourceManager;
            observer.beforeEachTestAfterResourceHandling(this.savedResource, nextTestMethod, testHistorie, testClass);
        }
    }

    /**
     * Inform the {@link #resourceManager} about the test case end.
     *
     * When the {@link #resourceManager} implements {@link ResouceManagerWithTestResultObserver} then it should be
     * informed about the test case end.
     *
     * @param testHistorie the test history - contains the executed methods and results - the last item was the last
     * executed one
     * @param testClass the test class
     */
    public void afterTest(final List<TestResult> testHistorie, final Object testClass) {
        if (this.resourceManager instanceof ResourceManagerWithTestResultObserver) {

            //unchecked is valid, because the savedResouce is delivered by the ResourceManager itself
            @SuppressWarnings({ "unchecked", "rawtypes" })
            ResourceManagerWithTestResultObserver<Object> observer = (ResourceManagerWithTestResultObserver) this.resourceManager;
            observer.afterTest(this.savedResource, testHistorie, testClass);
        }
    }

}
