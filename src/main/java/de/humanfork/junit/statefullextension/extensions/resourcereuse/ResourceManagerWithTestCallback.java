/*
 * Copyright 2018-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.statefullextension.extensions.resourcereuse;

/**
 * Combine {@link ResourceManagerWithBeforeTestObserver} and {@link ResouceManagerWithTestResultObserver}.
 * @author engelmann
 *
 * @param <T> the type of the managed resource
 */
public interface ResourceManagerWithTestCallback<T>
        extends ResourceManager<T>, ResourceManagerWithBeforeTestObserver<T>, ResourceManagerWithTestResultObserver<T> {

}
