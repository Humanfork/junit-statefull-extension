/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.statefullextension.extensions.resourcereuse;

/**
 * Runtime exception thrown by {@link ResourceReuseExtension}.
 * @author Ralph Engelmann
 *
 */
public class ResourceReuseException extends RuntimeException {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -9108033051707866083L;

    /**
     * Instantiates a new resource reuse exception.
     */
    public ResourceReuseException() {
        super();
    }

    /**
     * Instantiates a new resource reuse exception.
     *
     * @param message the message
     * @param cause the cause
     */
    public ResourceReuseException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new resource reuse exception.
     *
     * @param message the message
     */
    public ResourceReuseException(final String message) {
        super(message);
    }

    /**
     * Instantiates a new resource reuse exception.
     *
     * @param cause the cause
     */
    public ResourceReuseException(final Throwable cause) {
        super(cause);
    }

}
