/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.statefullextension.extensions.forward;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.TestInfo;

import de.humanfork.junit.statefullextension.StatefullTestLifeCycle;
import de.humanfork.junit.statefullextension.TestResult;
import de.humanfork.junit.statefullextension.util.AnnotationUtil;

/**
 * Scan for static annotated fields and forwarding all {@link StatefullTestLifeCycle} events to that instances.
 *
 * Scan for static fields by an annotation that is defined by the constructor parameter.
 * This annotated fields fields must have values (not null) that implements the {@link StatefullTestLifeCycle} interface.
 * All {@link StatefullTestLifeCycle} events will we forwarded to the instanced in this fields.
 *
 * @param <T> the annotation type
 *
 * @see de.humanfork.junit.statefullextension.extensions.forward package java doc
 * @author Ralph Engelmann
 */
public class AbstractInstanceBaseForwardingExtension<T extends Annotation> implements StatefullTestLifeCycle {

    /** The different handlers that will get the forwarded events. */
    private final List<StatefullTestLifeCycle> handlers;

    /**
     * Instantiates a new abstract instance base forwarding extension.
     *
     * @param clazz the test class
     * @param annotationClass the annotation used to determine the hanlders.
     */
    public AbstractInstanceBaseForwardingExtension(final Class<?> clazz, final Class<T> annotationClass) {
        this.handlers = scanForHandlers(clazz, annotationClass).collect(Collectors.toList());
    }

    @Override
    public void beforeAll() {
        for (StatefullTestLifeCycle handler : this.handlers) {
            handler.beforeAll();
        }
    }

    @Override
    public void afterAll() {
        for (StatefullTestLifeCycle handler : this.handlers) {
            handler.afterAll();
        }
    }

    @Override
    public void beforeEach(final TestInfo nextTestMethod, final List<TestResult> testHistorie,
            final Object testClass) {
        for (StatefullTestLifeCycle handler : this.handlers) {
            handler.beforeEach(nextTestMethod, testHistorie, testClass);
        }
    }

    @Override
    public void afterEach(final List<TestResult> testHistorie, final Object testClass) {
        for (StatefullTestLifeCycle handler : this.handlers) {
            handler.afterEach(testHistorie, testClass);
        }
    }

    /**
     * Scan for handlers.
     *
     * @param clazz the class where the scan is done
     * @param annotationClass the annotation to determine the handlers
     * @return the found handlers
     */
    private Stream<StatefullTestLifeCycle> scanForHandlers(final Class<?> clazz, final Class<T> annotationClass) {
        // @formatter:off
        return AnnotationUtil.scanForFields(clazz)
                .filter(AnnotationUtil.hasAnnotation(annotationClass))
                .filter(AnnotationUtil.isStatic())
                .map(AnnotationUtil.toValue(null, StatefullTestLifeCycle.class));
        // @formatter:on
    }
}
