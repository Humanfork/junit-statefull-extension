/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
/**
 * Statefull extension that allows to reuse the SAME resource in different tests.
 *
 * Typical use case.
 * <ol>
 *   <li> First step:
 *       Implement a class that implements the
 *       {@link de.humanfork.junit.statefullextension.extensions.resourcereuse.ResourceManager}
 *       interface
 *
 *       For example:
 *       <p>
 *       {@code
 *          public class WebDriverResourceManager implements ResourceManager<WebDriver>
 *       }
 *       </p>
 *   </li>
 *   <li> Second step:
 *      Create an annotation that use
 *      {@link de.humanfork.junit.statefullextension.extensions.resourcereuse.ResourceForReuse}
 *      as an meta annotation.
 *      The parameter
 *      {@link de.humanfork.junit.statefullextension.extensions.resourcereuse.ResourceForReuse#resourceManagerClass() ResourceForReuse.resourceManagerClass}
 *      must point to the in step 1 implemented resource manager class.
 *
 *      For example:
 *      <pre><code>
 *         {@literal @}Retention(RetentionPolicy.RUNTIME)
 *         {@literal @}Target({ ElementType.FIELD, ElementType.ANNOTATION_TYPE })
 *         {@literal @}ResourceForReuse(resourceManagerClass = WebDriverResourceManager.class)
 *         public {@literal @}interface WebDriverReuseResource{}
 *      </code></pre>
 *   </li>
 *   <li> Third step:
 *      Activate the Resource reusing Extension by annotation the test class with
 *      <ul>
 *          <li><code>{@literal @}RunWith(StatefullTestExtensionRunner.class)</code>and</li>
 *          <li><code>{@literal @}ResourceReusingExtension</code></li>
 *      </ul>
 *   </li>
 *   <li> 4. step:
 *       Add the resource that should be reused, together with the created annotation to the test
 *       <pre><code>
 *          {@literal @}WebDriverReuseResource
 *          public WebDriver driver;
 *       </code></pre>
 *   </li>
 * </ol>
 *
 * In the End the test should looks like
 * <pre><code>
 *   {@literal @}RunWith(StatefullTestExtensionRunner.class)
 *   {@literal @}ResourceReusingExtension
 *   public class ArticleControllerIT {
 *
 *       {@literal @}WebDriverReuseResource
 *       public WebDriver driver;
 *
 *       {@literal @}Test test1{...}
 *       {@literal @}Test test2{...}
 *       ...
 *   }
 * </code></pre>
 *
 */
package de.humanfork.junit.statefullextension.extensions.resourcereuse;
