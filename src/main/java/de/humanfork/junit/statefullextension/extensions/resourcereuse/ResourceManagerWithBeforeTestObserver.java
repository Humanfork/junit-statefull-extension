/*
 * Copyright 2018-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.statefullextension.extensions.resourcereuse;

import java.util.List;

import org.junit.jupiter.api.TestInfo;

import de.humanfork.junit.statefullextension.TestResult;

/**
 * A {@link ResourceManager} with an additional method that gets invoked right before each test,
 * to inform about the current test result.
 *
 * @author Ralph Engelmann
 *
 * @param <T> the type of the managed resource
 */
public interface ResourceManagerWithBeforeTestObserver<T> extends ResourceManager<T> {

    /**
     * This Method is invoked before every test case before the resource handline is done.
     *
     * This Method is invoked before the resource handling methods
     * ({@link #init(FrameworkMethod, Object) init},
     * {@link #reuseAfterSuccess(Object, FrameworkMethod, List, Object) reuseAfterSuccess}
     * {@link #reuseAfterFail(Object, FrameworkMethod, List, Object) reuseAfterFail} or
     * {@link #reinitialize(Object, FrameworkMethod, List, Object) reinitialize}) will been invoked.
     * Therefore the {@code currentState} parameter can in an inappropriate state (null).
     *
     * @param currentState the resource before the n-th test - attention this variable is unassigned (null) for the first test
     * @param nextTestMethod the next test method
     * @param testHistory the test history, contains the test methods and the results
     * @param testClass the test class
     */
    void beforeEachTestBeforeResourceHandling(T currentState, TestInfo nextTestMethod, List<TestResult> testHistory,
            Object testClass);

    /**
     * This Method is invoked before every test case after the resource handling is done.
     *
     * This Method is invoked after the resource handling methods
     * ({@link #init(FrameworkMethod, Object) init},
     * {@link #reuseAfterSuccess(Object, FrameworkMethod, List, Object) reuseAfterSuccess}
     * {@link #reuseAfterFail(Object, FrameworkMethod, List, Object) reuseAfterFail} or
     * {@link #reinitialize(Object, FrameworkMethod, List, Object) reinitialize}) are invoked.
     *
     * @param currentState the resource before the n-th test - attention this variable is unassigned (null) for the first test
     * @param nextTestMethod the next test method
     * @param testHistory the test history, contains the test methods and the results
     * @param testClass the test class
     */
    void beforeEachTestAfterResourceHandling(T currentState, TestInfo nextTestMethod, List<TestResult> testHistory,
            Object testClass);

}
