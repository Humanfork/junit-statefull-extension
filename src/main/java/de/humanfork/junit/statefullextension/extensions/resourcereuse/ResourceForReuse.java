/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.statefullextension.extensions.resourcereuse;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;

import org.junit.jupiter.api.TestInfo;

import de.humanfork.junit.statefullextension.TestResult;

/**
 * The resource will managed by an {@link ResourceManager} instance.
 * If no other {@link ResourceManager} is specified (by {@link #resourceManagerClass()}) than per default the instance
 * in a static field with name start with the same name like the field that is annotated by this annotation and with the
 * prefix <pre>ResourceManager</pre> will be used.
 *
 * For example:
 * <pre><code>
 * {@literal @} ReuseResource
 * private driver;
 *
 * public static driverResourceResourceManager = new MyDriverResourceManager();
 * </code></pre>
 *
 * If the parameter {@link #resourceManagerClass()} specify an {@link ResourceManager} class then an instance of this
 * class will be created to manage the resource.
 * <ol>
 *   <li>
 *      If the resource manager has a constructor with only one parameter of type class, then this constructor will be
 *      used to create an instance of the {@link ResourceManager}. Its parameter will be the test class.
 *   </li>
 *   <li>
 *     If this constructor does not exist, the constructor without parameter will be used.
 *   </li>
 * </ol>
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.ANNOTATION_TYPE })
public @interface ResourceForReuse {

    /**
     * Optional: Specify the class of the resource manager.
     * The Resource Extension will create an instance of this class.
     *
     * @return the class
     */
    Class<? extends ResourceManager<?>> resourceManagerClass() default NoResouceManager.class;

    /**
     * Marker to simulate {@code Null} in an annotation.
     * @author Ralph Engelmann
     */
    static final class NoResouceManager implements ResourceManager<Object> {

        @Override
        public Object init(final TestInfo nextTestMethod, final Object testClass) {
            return null;
        }

        @Override
        public Object reuseAfterSuccess(final Object currentState, final TestInfo nextTestMethod,
                final List<TestResult> testHistory, final Object testClass) {
            return null;
        }

        @Override
        public Object reuseAfterFail(final Object currentState, final TestInfo nextTestMethod,
                final List<TestResult> testHistory, final Object testClass) {
            return null;
        }

        @Override
        public Object cleanup(final Object currentState) {
            return null;
        }

        @Override
        public Object reinitialize(final Object currentState, final TestInfo nextTestMethod,
                final List<TestResult> testHistory, final Object testClass) {
            return null;
        }

    }
}
