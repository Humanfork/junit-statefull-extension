/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.statefullextension;

import java.util.List;

import org.junit.jupiter.api.TestInfo;

/**
 * The life cycle that is supported by {@link Junit5StatefullTestExtension}.
 * 
 * This interface is an extension of the similar interfaces {@link org.junit.jupiter.api.extension.BeforeAllCallback},
 * {@link org.junit.jupiter.api.extension.BeforeEachCallback},
 * {@link org.junit.jupiter.api.extension.AfterEachCallback} and 
 * {@link org.junit.jupiter.api.extension.AfterAllCallback} in that way that this interface provides an
 * history about the already executed tests.
 * 
 * @author Ralph Engelmann
 */
public interface StatefullTestLifeCycle {

    /**
     * Invoked right before the methods annotated by {@link org.junit.jupiter.api.BeforeAll} are invoked.
     * There is no concrete instance of the test class instantiated yet.
     */
    void beforeAll();

    /**
     * Invoked right after the methods annotated by {@link org.junit.jupiter.api.AfterAll} are invoked.
     */
    void afterAll();

    /**
     * Invoked right before the methods annotated by {@link  org.junit.jupiter.api.BeforeEach} are invoked.
     *
     * @param nextTestMethod the next test method that will run
     * @param testHistory the test history - the result of all so far done tests, the last test is the last item
     * @param testClass the instance of the test class
     */
    void beforeEach(TestInfo nextTestMethod, List<TestResult> testHistory, Object testClass);

    /**
     * Invoked right after the methods annotated by {@link  org.junit.jupiter.api.AfterEach} are invoked.
     *
     * @param testHistory the test history - the result of all so far done tests, the last test (that was finished
     * right now) is the last item
     * @param testClass the instance of the test class
     */
    void afterEach(List<TestResult> testHistory, Object testClass);

}
