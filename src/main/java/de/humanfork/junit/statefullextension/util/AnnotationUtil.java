/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.statefullextension.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;

/** Util class to handle annotations. */
public final class AnnotationUtil {

    /** Util classes need no constructor. */
    private AnnotationUtil() {
        super();
    }

    /**
     * Search for an annotation, and respect meta annotations too.
     *
     * So if an annotation has other annotations, they will be taken in account too.
     *
     * @param <T> annotation type
     * @param searchFor the searched annotation class
     * @param annotations a list of annotations
     * @return the concrete found annotation or {@code null} if there was no annotation of this class.
     */
    public static <T extends Annotation> T findOneMetaAnnotation(final Class<T> searchFor,
            final Annotation... annotations) {
        HashSet<Class<? extends Annotation>> scannedAnnotations = new HashSet<>();
        for (Annotation annotation : annotations) {
            T found = findOneMetaAnnotation(searchFor, annotation, scannedAnnotations);
            if (found != null) {
                return found;
            }
        }
        return null;
    }

    /**
     * Search for an annotation, and respect meta annotations too.
     *
     * Return the annotation if it is of the required type.
     * If not is search in the meta annotations of the annotations recursively.
     *
     * @param <T> annotation type
     * @param searchFor the searched annotation class
     * @param annotation  the annotation
     * @param done already done annotation classes
     * @return the concrete found annotation or {@code null} if there was no annotation of this class.
     */
    private static <T extends Annotation> T findOneMetaAnnotation(final Class<T> searchFor, final Annotation annotation,
            final HashSet<Class<? extends Annotation>> done) {

        boolean notDoneYet = done.add(annotation.annotationType());
        if (notDoneYet) {
            if (annotation.annotationType().equals(searchFor)) {
                /** this cast is save */
                @SuppressWarnings("unchecked")
                T found = (T) annotation;
                return found;
            }
            for (Annotation meta : annotation.annotationType().getAnnotations()) {

                T found = findOneMetaAnnotation(searchFor, meta, done);
                if (found != null) {
                    return found;
                }
            }
        }
        return null;
    }

    /**
     * Search for all annotations of an specific class, and respect meta annotations too.
     *
     * So if an annotation has other annotations, they will be taken in account too.
     *
     * <b>If an annotation is recursive (link {@link java.lang.annotation.Documented})
     * then the outer and the first recursive instance will be taken in account.</b>
     * This mean
     * <pre><code>
     *   {@literal @}Recursive("inner")
     *   public {@literal @}interface Recursive {
     *     String value();
     *   }
     *
     *   {@literal @}Recursive("outer")
     *   public @interface Context {}
     *
     *   findAllMetaAnnotation(Recursive.class, instanceOfContext) =
     *   {
     *      Recursive(value="outer")
     *      Recursive(value="inner")
     *   }
     * </code></pre>
     *
     * @param <T> annotation type
     * @param searchFor the searched annotation class
     * @param annotations a list of annotations
     * @return the concrete found annotation or {@code null} if there was no annotation of this class.
     */
    public static <T extends Annotation> List<T> findAllMetaAnnotation(final Class<T> searchFor,
            final Annotation... annotations) {
        List<T> result = new ArrayList<>();
        HashSet<Class<? extends Annotation>> scannedAnnotations = new HashSet<>();
        for (Annotation annotation : annotations) {
            result.addAll(findAllMetaAnnotation(searchFor, annotation, scannedAnnotations));
        }
        return result;
    }

    /**
     * Search for all annotations of an specific class, and respect meta annotations too.
     *
     * Return the annotation if it is of the required type.
     * If not is search in the meta annotations of the annotations recursively.
     *
     * @param <T> annotation type
     * @param searchFor the searched annotation class
     * @param annotation  the annotation
     * @param done already done annotation classes
     * @return the concrete found annotation or {@code null} if there was no annotation of this class.
     */
    private static <T extends Annotation> List<T> findAllMetaAnnotation(final Class<T> searchFor,
            final Annotation annotation, final HashSet<Class<? extends Annotation>> done) {

        List<T> result = new ArrayList<>();

        if (annotation.annotationType().equals(searchFor)) {
            /** this cast is save */
            @SuppressWarnings("unchecked")
            T found = (T) annotation;
            result.add(found);
        }
        boolean notDoneYet = done.add(annotation.annotationType());
        if (notDoneYet) {
            for (Annotation meta : annotation.annotationType().getAnnotations()) {
                result.addAll(findAllMetaAnnotation(searchFor, meta, done));
            }
        }
        return result;
    }

    /**
     * Build a stream that contains all declared fields of the given class and its superclass.
     * @param clazz the class to analyze
     */
    public static Stream<Field> scanForFields(final Class<?> clazz) throws IllegalArgumentException {
        Objects.requireNonNull(clazz, "argument clazz must not been null");

        Class<?> cursorClass = clazz;
        Builder<Field> fieldStreamBuilder = Stream.builder();
        do {
            for (Field field : cursorClass.getDeclaredFields()) {
                fieldStreamBuilder.add(field);
            }
            cursorClass = cursorClass.getSuperclass();
        } while ((cursorClass != null) && (cursorClass != Object.class));

        return fieldStreamBuilder.build();
    }

    /**
     * Build a stream that contains all declared fields of the given class and its superclass.
     * @param clazz the class to analyze
     */
    public static Stream<Field> scanForFields(final Class<?> clazz, final Class<? extends Annotation> annotationClass)
            throws IllegalArgumentException {
        return scanForFields(clazz).filter(hasAnnotation(annotationClass));
    }

    /**
     * Provides an {@link Predicate} for {@link Field}s that match all fields that are (direct) annotated
     * with the given annotation.
     *
     * @param annotationClass the searched annotation class
     * @return a predicate that select all fields annotated with the given annotation
     */
    public static Predicate<Field> hasAnnotation(final Class<? extends Annotation> annotationClass) {
        return (final Field f) -> f.getAnnotation(annotationClass) != null;
    }
    
    /**
     * Provides an {@link Predicate} for {@link Field}s that match all fields that are direct or indirect (via meta
     * annotation) annotated with the given annotation.
     *
     * @param annotationClass the searched annotation class
     * @return a predicate that select all fields annotated with the given (meta) annotation
     */
    public static Predicate<Field> hasMetaAnnotation(final Class<? extends Annotation> annotationClass) {
        return (final Field f) -> AnnotationUtil.findOneMetaAnnotation(annotationClass, f.getAnnotations()) != null;
    }
    

    /**
     * Provides an {@link Predicate} for {@link Field}s that match for static fields
     * @return predicate that match for static fields
     */
    public static Predicate<Field> isStatic() {
        return (final Field f) -> Modifier.isStatic(f.getModifiers());
    }

    public static <T> Function<Field, T> toValue(final Object target, final Class<T> expectedType) {
        //o can be null for static fields
        return (final Field f) -> expectedType.cast(getField(f, target));
    }

    @SuppressWarnings("unchecked")
    public static <T> Function<Field, T> toValue(final Object target) {
        //that is not save, prefer to use toValue(Object o, Class<T> expectedType) with explicit expected Type.
        return (Function<Field, T>) toValue(target, Object.class);
    }

    /**
     * Get the field represented by the supplied {@link Field field object} on the
     * specified {@link Object target object}. In accordance with {@link Field#get(Object)}
     * semantics, the returned value is automatically wrapped if the underlying field
     * has a primitive type.
     * <p>Thrown exceptions are handled via a call to {@link #handleReflectionException(Exception)}.
     * @param field the field to get
     * @param target the target object from which to get the field
     * @return the field's current value
     */
    private static Object getField(final Field field, final Object target) {
        try {
            return field.get(target);
        } catch (IllegalAccessException ex) {
            String targetDescription;
            if (target != null) {
                try {
                    targetDescription = "from object: " + target.toString();
                } catch (Exception e) {
                    targetDescription = "from object: <error while accessing target object: " + e.getClass() + " : "
                            + e.getMessage() + ">";
                }
            } else {
                targetDescription = "from NULL (static fields only)";
            }

            throw new IllegalStateException(
                    "Unexpected reflection exception - while access field " + field + " " + targetDescription,
                    ex);
        }
    }
    
    /**
     * Try to find a {@link Field field} in the given class thats name match the argument fieldName.
     * All fields (private, ... public) and superclass are considered.
     * 
     * @param clazz the class to introspect
     * @param fieldName the name of the field
     * 
     * @return the corresponding Field object
     */
    public static Optional<Field> findField(Class<?> clazz, String fieldName) {
        Objects.requireNonNull(clazz, "Agument clazz must not been null");
        Objects.requireNonNull(fieldName, "Agument fieldName must not been null");
        
        Class<?> searchType = clazz;
        while (!Object.class.equals(searchType) && searchType != null) {
            for (Field field : searchType.getDeclaredFields()) {
                if (field.getName().equals(fieldName)) {
                    return Optional.of(field);
                }
            }
            searchType = searchType.getSuperclass();
        }
        return Optional.empty();
    }

}
