/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.junit.statefullextension;

import java.util.Objects;

import org.junit.jupiter.api.TestInfo;

/**
 * The result of an single test.
 * @author Ralph Engelman
 */
public class TestResult {

    /** The method that was tested. */
    private final TestInfo method;

    /** A Exception that was thrown, or null if there was no. */
    private final Throwable failure;

    /**
     * Instantiates a new test result.
     *
     * @param method the method
     * @param failure the failure
     */
    public TestResult(final TestInfo method, final Throwable failure) {
        Objects.requireNonNull(method, "parameter method must not be null");

        this.method = method;
        this.failure = failure;
    }

    /**
     * Instantiates a new test result.
     *
     * @param method the method
     */
    public TestResult(final TestInfo method) {
        this(method, null);
    }

    /**
     * Gets the method.
     *
     * @return the method
     */    
    public TestInfo getMethod() {
        return this.method;
    }

    /**
     * Gets the failure.
     *
     * @return the failure or null.
     */
    public Throwable getFailure() {
        return this.failure;
    }

    /**
     * Checks if is failure.
     *
     * @return true, if is failure
     */
    public boolean isFailure() {
        return this.failure != null;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TestResult [name=" + this.method.getDisplayName() + ", failure=" + this.failure + "]";
    }

}
