Usage
=====

Implement the interface `de.humanfork.junit.statefullextension.extensions.resourcereuse.ResourceManager`.
For example by an class `MyResourceManager implements ResourceManager<MyResource>`

Add this two annotations to the Test Class

    @ExtendWith(Junit5StatefullTestExtension.class)
    @StatefullTestExtension(ResourceReuseExtension.class)

Add an (none static) field that type matches the type provides by your `ResourceManager` to the Test Class and annotate it with `@ResourceForReuse(MyResourceManager)`:

    @ResourceForReuse
    public MyResource myResource;
    
Now the `ResourceReuseExtension` will invoke your Resource Manager and inject its provided instances into the field `myResource`. 

## Lowlevel: `StatefullTestLifeCycle`

Implement the interface `de.humanfork.junit.statefullextension.StatefullTestLifeCycle`.
For example by an class `SomeDemo`.
And add this to annotations to the Test Class

    @ExtendWith(Junit5StatefullTestExtension.class)
    @StatefullTestExtension(SomeDemo.class)
    
Then `SomeDemo` become invoked before and after each test of the annotated Test-Class.
